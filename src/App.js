import { BrowserRouter, Route, Routes } from "react-router-dom";
import useRout from "./Routes";
import { useSelector } from "react-redux";
import Login from "./Pages/login/Login";
import "./styled/style.scss";

function App() {
  let { loading, error, authdata } = useSelector(
    (state) => state.persistTheme.authSlice
  );

  let { dark } = useSelector((state) => state.persistTheme.themeslice);
  const routes = useRout(authdata && authdata.data.token);

  return (
    <div className={dark ? "app dark" : "app"}>
      <BrowserRouter>
        <Routes>
          {routes.map((route, index) => (
            <Route
              key={index}
              path={route.path}
              element={
                authdata && authdata.data.token ? route.element : <Login />
              }
            />
          ))}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
