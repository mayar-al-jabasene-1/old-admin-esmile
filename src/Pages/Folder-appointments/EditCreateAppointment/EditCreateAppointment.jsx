import React, { Fragment, useEffect, useRef, useState } from "react";
import Sidebar from "../../../components/Sidebar/Sidebar";
import Navbar from "../../../components/Navbar/Navbar";
import ComboBox from "../../../components/AutoComplate/ComboBox";
import { getusers } from "../../../store/userslice";
import { useDispatch, useSelector } from "react-redux";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";

import ResponsiveDateTimePickers from "../../../components/ResponsiveDateTimePickers/ResponsiveDateTimePickers";
import dayjs from "dayjs";
import { Editor } from "@tinymce/tinymce-react";
import "./EditCreateAppointment.scss";
import {
  createApoointment,
  resetDataCreateAction,
  resetDataUpdateAction,
} from "../../../store/appointmentSlice";
import { useNavigate, useParams } from "react-router-dom";
import { getAllDoctors, getDoctorById } from "../../../store/DoctorSlice";
import TimePickerViewRenderers from "../../../components/TimePicker/TimePickerViewRenderers";
import { notifyError } from "../../../Notification";
function EditCreateAppointment() {
  const editorRef = useRef(null);
  const titleref = useRef(null);

  const handleEditorChange = (content, editor) => {
    setSelectDetails(content);
  };

  let { name } = useParams();

  const [selectedDate, setSelectedDate] = useState("");
  let [selectedTime, setSelectedTime] = useState("");
  const [selectname, setSelectName] = useState(null);
  const [selectdoctor, setSelectDoctor] = useState(null);
  const [selectdetails, setSelectDetails] = useState(null);

  let handleInputChange = (event, inputName) => {
    setSelectedTime(event);
  };

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getusers());
    dispatch(getAllDoctors());
  }, []);

  let { data, loading, error } = useSelector((state) => state.users);
  let {
    data: doctors,
    loading: loadingDoctors,
    dataSingle: SingleDoctor,
    laodingBTN,
  } = useSelector((state) => state.doctors);

  let {
    loading: loadoingappo,
    laodingApo,
    dataSingleAppo,
    dataCreate,
  } = useSelector((state) => state.appointments);

  let dataSend = {
    user_id: selectname && selectname.value,
    selected_date: selectedDate && selectedDate.format("YYYY-MM-DD"),
    selected_time: selectedTime,
    note: selectdetails,
  };
  let sendData = (e) => {
    e.preventDefault();
    if (!dataSend.selected_time) {
      notifyError("Field Time Is Required");
    }
    if (!dataSend.selected_date) {
      notifyError("Field Date Is Required");
    }
    if (!dataSend.note) {
      notifyError("Field Descreption Is Required");
    }
    if (!selectdoctor) {
      notifyError("Field Doctor Is Required");
    }
    if (!selectname) {
      notifyError("Field Patient Is Required");
    }
    if (
      dataSend.user_id &&
      dataSend.selected_time &&
      dataSend.selected_date &&
      selectdoctor
    ) {
      dispatch(
        createApoointment({
          data: dataSend,
          id: dataSend.user_id,
          doctor_id: selectdoctor && selectdoctor.value,
        })
      );
    }
  };

  useEffect(() => {
    if (selectdoctor) {
      dispatch(getDoctorById(selectdoctor.value));
    }
  }, [selectdoctor]);

  const path = useNavigate();

  useEffect(() => {
    if (dataCreate) {
      dispatch(resetDataCreateAction(false));
      dispatch(resetDataUpdateAction(false));
      path("/appointments");
    }
  }, [dataCreate]);

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="new-user">
          <h2 className="title">
            {name === "add" ? "Add New Appointment" : "Edit Appointment"}
          </h2>

          <form className="Details" onSubmit={(e) => sendData(e)}>
            <div className="one">
              <p className="pp">Appointment Details:</p>
            </div>
            <div className="row">
              <div className="col-lg-6 col-sm-6 mb-4 datat">
                <div>
                  {loading ? (
                    <CircularIndeterminate />
                  ) : (
                    <Fragment>
                      <div className="mb-3">
                        <label htmlFor="detail">
                          Patient Name :<sup className="text-danger">*</sup>
                        </label>
                      </div>
                      <ComboBox
                        data={data}
                        type="patients"
                        dataselected={setSelectName}
                      />
                    </Fragment>
                  )}
                </div>
              </div>

              <div className="col-lg-6 col-sm-6 mb-4">
                <div>
                  {loadingDoctors ? (
                    <CircularIndeterminate />
                  ) : (
                    <Fragment>
                      <div className="mb-3">
                        <label htmlFor="detail">
                          Doctor Name:<sup className="text-danger">*</sup>
                        </label>
                      </div>
                      <ComboBox
                        data={doctors && doctors}
                        type="doctors"
                        dataselected={setSelectDoctor}
                      />
                    </Fragment>
                  )}
                </div>
              </div>

              <div className="col-lg-6 col-sm-6 mb-4">
                {laodingBTN ? (
                  <CircularIndeterminate />
                ) : (
                  <Fragment>
                    <div className="mb-2">
                      <label htmlFor="detail">
                        Date:<sup className="text-danger">*</sup>
                      </label>
                    </div>
                    <div>
                      <ResponsiveDateTimePickers
                        setSelectedDate={setSelectedDate}
                        selectedDate={selectedDate}
                        SingleDoctor={SingleDoctor && SingleDoctor["0"]}
                      />
                    </div>
                  </Fragment>
                )}
              </div>

              <div className="col-lg-6 col-sm-6 mb-4">
                {laodingApo ? (
                  <CircularIndeterminate />
                ) : (
                  <Fragment>
                    <div className="">
                      <label htmlFor="detail">
                        Time:<sup className="text-danger">*</sup>
                      </label>
                    </div>
                    <div>
                      <TimePickerViewRenderers
                        getdataAppo={handleInputChange}
                        type="start"
                        value={""}
                        name={"add"}
                        SingleDoctor={SingleDoctor && SingleDoctor["0"]}
                        SelDate={selectedDate}
                        dataSingleAppo={dataSingleAppo}
                      />
                    </div>
                  </Fragment>
                )}
              </div>

              <div className="col-lg-12 col-sm-12 mt-5 appo mb-3">
                <div className="dets-inp">
                  <label htmlFor="detail">
                    Detail:<sup className="text-danger">*</sup>
                  </label>
                </div>

                <div className="col-lg-12">
                  <Editor
                    apiKey="zvo8oa2gmt3hxn4sp8v14uzgoe3u26bbufnedvo08zg35gqa"
                    onInit={(evt, editor) => (editorRef.current = editor)}
                    initialValue=""
                    required
                    init={{
                      height: 500,
                      menubar: true,
                      plugins: [
                        "advlist",
                        "autolink",
                        "lists",
                        "link",
                        "image",
                        "charmap",
                        "preview",
                        "anchor",
                        "searchreplace",
                        "visualblocks",
                        "code",
                        "fullscreen",
                        "insertdatetime",
                        "media",
                        "table",
                        "code",
                        "help",
                        "wordcount",
                      ],
                      toolbar:
                        "undo redo | blocks | " +
                        "bold italic forecolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | help",
                      content_style:
                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                    }}
                    onEditorChange={handleEditorChange}
                  />
                </div>
              </div>
            </div>

            {loadoingappo ? (
              <button
                disabled
                className="submit btn view"
                style={{
                  cursor: "no-drop",
                }}
              >
                loading...
              </button>
            ) : name === "add" ? (
              <button className="submit btn view">Submit</button>
            ) : (
              <button className="submit btn view">Update</button>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}

export default EditCreateAppointment;
