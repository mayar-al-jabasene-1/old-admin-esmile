import React, { useEffect, useState } from "react";
import Navbar from "../../../components/Navbar/Navbar";
import Sidebar from "../../../components/Sidebar/Sidebar";
import ModalDelete from "../../../components/ModalDelete/ModalDelete";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { getAllappointment } from "../../../store/appointmentSlice";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import Datatable from "../../../components/datatable/Datatable";
import { BASE_URL } from "../../../apiConfig";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import EditIcon from "@mui/icons-material/Edit";
import ModalMulti from "../../../components/ModalMulti/ModalMulti";
import ClearableProp from "../../../components/DatePicker/ClearableProp";
import dayjs from "dayjs";
import "./MainAppointments.scss";
import ClearIcon from "@mui/icons-material/Clear";
import DoneIcon from "@mui/icons-material/Done";
import * as XLSX from "xlsx";
import jsPDF from "jspdf"; // Import the jsPDF library
import "jspdf-autotable"; // Import the jspdf-autotable plugin
import SettingsIcon from "@mui/icons-material/Settings";

function MainAppointments() {
  // State variable to store selected row IDs
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
  };

  let dispatch = useDispatch();
  let navigation = useNavigate();

  useEffect(() => {
    dispatch(getAllappointment());
  }, []);

  let { loading, data, error } = useSelector((state) => state.appointments);
  let gotonewfn = (id, name) => {
    if (name === "add") {
      navigation("/appointments/add");
    }
    if (name === "edit") {
      navigation(`/appointments/edit/${id}`);
    }
    if (name === "show") {
      navigation(`/appointments/show/${id}`);
    }
  };

  const ColumnsAppointment = [
    { field: "id", headerName: "ID", width: 80 },
    {
      field: "user",
      headerName: "Patient",
      width: 270,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.user_picture ? (
              <img
                className="cellImg"
                src={`${BASE_URL}/storage/${params.row.user_picture}`}
                alt="avatar"
              />
            ) : (
              <div className="boxImageChar">
                <span> {params.row.first_name.slice(0, 1)}</span>
              </div>
            )}

            {params.row.first_name + " " + params.row.last_name}
          </div>
        );
      },
    },
    {
      field: "selected_date",
      headerName: "Date",
      width: 130,
      renderCell: (params) => {
        return <div className="remaining">{params.row.selected_date}</div>;
      },
    },
    {
      field: "selected_time",
      headerName: "Time",
      width: 140,
      sortable: "asc",
      renderCell: (params) => {
        return <div className="remaining">{params.row.selected_time}</div>;
      },
    },
    {
      field: "gender",
      headerName: "Gender",
      sortable: false,
      width: 130,
    },
    {
      field: "doctor_first_name",
      headerName: "Doctor",
      width: 200,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.doctor.doctor_picture ? (
              <img
                className="cellImg"
                src={`${BASE_URL}/storage/${params.row.doctor.doctor_picture}`}
                alt="avatar"
              />
            ) : (
              <div className="boxImageChar">
                <span>
                  {params.row.doctor.first_name.slice(0, 1).toUpperCase()}
                </span>
              </div>
            )}

            {params.row.doctor.first_name + " " + params.row.doctor.last_name}
          </div>
        );
      },
    },

    {
      field: "Action",
      headerName: "Action",
      sortable: false,
      filter: false,
      width: 160,
      renderCell: (params) => {
        return (
          <div className="box-action d-flex gap-3">
            <RemoveRedEyeIcon
              className="icon-show"
              onClick={(e) => gotonewfn(params.row.user_id, "show")}
            />
            {/* <EditIcon
              onClick={(e) => gotonewfn(params.id, "edit")}
              className="icon-edit"
        />*/}
            <ModalMulti params={params} text="appointment" type="show" />
            <ModalDelete text="appointment" params={params} />
          </div>
        );
      },
    },
  ];

  const [selectedDate, setSelectedDate] = useState(dayjs());
  const dayName = selectedDate.format("dddd");

  // Filter the data based on selectedDate
  const filteredData =
    data &&
    data.filter((item) => {
      // Format selectedDate to match the format of selected_time
      const formattedSelectedDate = selectedDate.format("MM/DD/YYYY");
      // Compare the formatted date with the selected_time
      return formattedSelectedDate === item.selected_date;
    });

  let [toggle, setToggle] = useState(true);

  let [togglePrint, setTogglePrint] = useState(false);
  const [isCopied, setIsCopied] = useState(false);

  // Add a function to handle XLSX export

  const handleExportXLSX = () => {
    const dataForExport = data.map((row) => {
      // Create a copy of the row without "user_picture" and "doctor_picture" columns
      const { user_picture, doctor_picture, ...rowData } = row;
      return rowData;
    });

    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataForExport);
    XLSX.utils.book_append_sheet(wb, ws, "Appointment Data");
    XLSX.writeFile(wb, "Appointment_data.xlsx");
  };

  // Add a function to handle CSV export
  const handleExportCSV = () => {
    const csvContent =
      "data:text/csv;charset=utf-8," + encodeURI(generateCSV());

    const link = document.createElement("a");
    link.setAttribute("href", csvContent);
    link.setAttribute("download", "Appointment_data.csv");
    link.click();
  };

  // Function to generate CSV data
  const generateCSV = () => {
    const columnsToExport = Object.keys(data[0]).filter(
      (col) => col !== "user_picture" && col !== "doctor_picture"
    );

    // Generate header row
    let csvData = columnsToExport.join(",") + "\n";

    // Generate data rows
    data.forEach((row) => {
      const rowData = columnsToExport.map((col) => row[col]).join(",");
      csvData += rowData + "\n";
    });

    return csvData;
  };
  // Add a function to handle PDF export
  const handleExportPDF = () => {
    // Create a copy of the data with the "last_name" field and without the "Action" column
    const dataForPDF = data.map((row) => {
      const { Action, ...rowData } = row;
      rowData.last_name = row.last_name;
      return rowData;
    });

    const doc = new jsPDF();
    doc.text("Appointment Data", 10, 10);

    // Define the columns directly without accessing userColumns
    const columns = [
      "id",
      "first_name",
      "last_name",
      "gender",
      "selected_date",
      "selected_time",
      "doctor_first_name",
    ];

    const rows = dataForPDF.map((row) =>
      columns.map((col) => {
        if (col === "doctor_first_name") {
          // Combine the first name and last name of the doctor
          return row.doctor.first_name + " " + row.doctor.last_name;
        }
        return row[col];
      })
    );

    // Create headers for the columns
    const columnHeaders = columns.map((col) => {
      const columnConfig = ColumnsAppointment.find(
        (column) => column.field === col
      );
      return columnConfig ? columnConfig.headerName : col;
    });

    doc.autoTable({
      head: [columnHeaders],
      body: rows,
    });

    doc.save("Appointment_data.pdf");
  };

  // Add a function to handle copying data to clipboard
  const handleCopyToClipboard = () => {
    const columns = Object.keys(data[0]);
    const rows = data.map((row) => columns.map((col) => row[col]));

    const copyText = rows.map((row) => row.join("\t")).join("\n");

    navigator.clipboard
      .writeText(copyText)
      .then(() => {
        console.log("Data copied to clipboard");
        setIsCopied(true); // Set the state to show the success message
        setTimeout(() => {
          setIsCopied(false); // Hide the success message after a delay
        }, 3000); // Hide after 3 seconds
      })
      .catch((error) => {
        console.error("Failed to copy data:", error);
      });
  };

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="two-box">
          <div className="appointments">
            <div className="header-top row">
              <div className=" col-md-6 col-lg-2 text-center mb-3">
                <h3>Appointments</h3>
              </div>
              <div className=" col-md-6 col-lg-6 mb-3">
                <div className="date-box">
                  <p className="me-3"> {dayName}</p>
                  <div className="date-pop">
                    {toggle && (
                      <ClearIcon
                        className="icon-toggle"
                        onClick={() => setToggle(false)}
                      />
                    )}
                    {!toggle && (
                      <DoneIcon
                        className="icon-toggle"
                        onClick={() => setToggle(true)}
                      />
                    )}
                    <ClearableProp
                      setSelectedDate={setSelectedDate}
                      selectedDate={selectedDate}
                    />
                  </div>
                </div>
              </div>
              <div className=" col-md-6 col-lg-4 mb-3">
                <div className="btn-list">
                  <div
                    onClick={() => setTogglePrint(!togglePrint)}
                    className="icon-list-print me-4"
                  >
                    <SettingsIcon />
                    <div className={togglePrint ? "show-print" : "d-none"}>
                      <span onClick={handleCopyToClipboard}>Copy</span>
                      <span onClick={handleExportCSV}>CSV</span>
                      <span onClick={handleExportXLSX}>Excel</span>
                      <span onClick={handleExportPDF}>PDF</span>
                    </div>
                  </div>
                  {isCopied && (
                    <div
                      id="datatables_buttons_info"
                      className="dt-button-info"
                    >
                      <h2>Copy to clipboard</h2>
                      <div>
                        Copied {Object.keys(data[0]).length} rows to clipboard
                      </div>
                    </div>
                  )}
                  <button
                    className="btn btn-add"
                    onClick={(e) => gotonewfn("", "add")}
                  >
                    Add New
                  </button>
                  <button className="btn btn-delete">
                    <ModalDelete
                      text="appointment"
                      params={data}
                      type="all"
                      selectedRowIds={selectedRowIds}
                    />
                  </button>
                </div>
              </div>
            </div>
            <div className="data-table">
              {loading ? (
                <div className="loading">
                  <CircularIndeterminate />
                </div>
              ) : (
                <Datatable
                  userColumns={ColumnsAppointment}
                  userRows={
                    toggle ? filteredData && filteredData : data && data
                  }
                  onSelectionModelChange={handleSelectionChange}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainAppointments;
