import React, { Fragment, useEffect } from "react";
import "./ShowAppointment.scss";
import { useParams } from "react-router-dom";
import Navbar from "../../../components/Navbar/Navbar";
import Sidebar from "../../../components/Sidebar/Sidebar";
import { useDispatch, useSelector } from "react-redux";
import { getuserappointment } from "../../../store/appointmentSlice";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import PhoneIcon from "@mui/icons-material/Phone";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import EventIcon from "@mui/icons-material/Event";
import { BASE_URL } from "../../../apiConfig";
import Datatable from "../../../components/datatable/Datatable";
import ModalDelete from "../../../components/ModalDelete/ModalDelete";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import ModalMulti from "../../../components/ModalMulti/ModalMulti";
function ShowAppointment() {
  let { id } = useParams();
  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getuserappointment(id));
  }, []);
  let { loadingSingle, datasingle, note, dataNote } = useSelector(
    (state) => state.appointments
  );

  //fucntion for calc age
  const calculateAge = (birthday) => {
    const currentDate = new Date();
    const birthdate = new Date(birthday);
    let age = currentDate.getFullYear() - birthdate.getFullYear();

    // Check if the birthday hasn't occurred yet this year
    if (
      currentDate.getMonth() < birthdate.getMonth() ||
      (currentDate.getMonth() === birthdate.getMonth() &&
        currentDate.getDate() < birthdate.getDate())
    ) {
      age--;
    }

    return age;
  };

  const ColumnsAppointment = [
    { field: "id", headerName: "ID", width: 80, sortable: "desc" },
    {
      field: "user",
      headerName: "User",
      width: 300,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.user_picture ? (
              <img
                className="cellImg"
                src={`${BASE_URL}/storage/${params.row.user_picture}`}
                alt="avatar"
              />
            ) : (
              <div className="boxImageChar">
                <span> {params.row.first_name.slice(0, 1)}</span>
              </div>
            )}

            {params.row.first_name + " " + params.row.last_name}
          </div>
        );
      },
    },
    {
      field: "gender",
      headerName: "Gender",
      sortable: false,
      width: 130,
    },
    {
      field: "doctor_first_name",
      headerName: "Doctor",
      width: 220,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.doctor.doctor_picture ? (
              <img
                className="cellImg"
                src={`${BASE_URL}/storage/${params.row.doctor.doctor_picture}`}
                alt="avatar"
              />
            ) : (
              <div className="boxImageChar">
                <span>
                  {" "}
                  {params.row.doctor.first_name.slice(0, 1).toUpperCase()}
                </span>
              </div>
            )}

            {params.row.doctor.first_name + " " + params.row.doctor.last_name}
          </div>
        );
      },
    },
    {
      field: "selected_time",
      headerName: "Date",
      width: 130,
      renderCell: (params) => {
        return <div className="remaining">{params.row.selected_date}</div>;
      },
    },
    {
      field: "Time",
      headerName: "Time",
      width: 120,
      renderCell: (params) => {
        return <div className="remaining">{params.row.selected_time}</div>;
      },
    },

    {
      field: "Action",
      headerName: "Action",
      sortable: false,
      filter: false,
      width: 160,
      renderCell: (params) => {
        return (
          <div className="box-action d-flex gap-3">
            <ModalMulti params={params} text="appointment" type="show" />
            <ModalDelete text="appointment" params={params} />
          </div>
        );
      },
    },
  ];

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-lg-10 singleuser sing-appo">
        <div className="two">
          {loadingSingle ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <Fragment>
              {datasingle && (
                <div className="col-lg-12" key={datasingle[0].id}>
                  <div className="box-user">
                    <div className="box-img text-center ss">
                      {datasingle[0].user_picture ? (
                        <img
                          src={`${BASE_URL}/storage/${datasingle[0].user_picture}`}
                          alt="..."
                        />
                      ) : (
                        <div className="boxImageChar">
                          <span>
                            {datasingle[0].first_name.slice(0, 1).toUpperCase()}
                          </span>
                        </div>
                      )}
                    </div>

                    <div className="title">
                      <h3 className="text-center mb-4">{`${datasingle[0].first_name} ${datasingle[0].last_name}`}</h3>
                      <div className="detailsitem d-flex justify-content-between mb-4 mt-4 gap-3 text-center ">
                        <span className="itemvalue">
                          <PeopleAltIcon className="icon" />{" "}
                          {datasingle[0].gender}
                        </span>
                        <span className="itemvalue">
                          <PhoneIcon className="icon" />{" "}
                          {datasingle[0].phone_number}
                        </span>
                        <span className="itemvalue">
                          <LocationOnIcon className="icon" />
                          {`${datasingle[0].location} / ${datasingle[0].location_details}`}
                        </span>
                        <span className="itemvalue">
                          <EventIcon className="icon" />{" "}
                          {calculateAge(datasingle[0].birthday)} years
                        </span>
                      </div>
                      <div className="d-flex mt-3">
                        <button className="btn btn-outline-primary view mt-2">
                          Message
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="data pt-3">
                {loadingSingle ? (
                  <div className="loading">
                    <CircularIndeterminate />
                  </div>
                ) : (
                  <Datatable
                    userColumns={ColumnsAppointment}
                    userRows={datasingle && datasingle}
                  />
                )}
              </div>
            </Fragment>
          )}
        </div>
      </div>
    </div>
  );
}

export default ShowAppointment;
