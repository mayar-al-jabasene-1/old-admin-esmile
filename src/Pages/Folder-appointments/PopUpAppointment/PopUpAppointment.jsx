import React, { Fragment } from "react";
import "./PopUpAppointment.scss";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import DoNotDisturbAltIcon from "@mui/icons-material/DoNotDisturbAlt";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
function PopUpAppointment({ type, data, handleClose }) {
  let title = data.row.note;

  let textContent = "";

  if (title) {
    const parser = new DOMParser();
    const doc = parser.parseFromString(title, "text/html");
    textContent = doc.body.textContent;
  }
  return (
    <Fragment>
      <Typography id="transition-modal-title" variant="h6" component="h2">
        <div className="head">
          <div className="title-head">
            <span>Note</span>
            <CloseIcon className="icon" onClick={handleClose} />
          </div>
        </div>
      </Typography>

      <Typography
        id="transition-modal-description"
        className="cate-modal"
        sx={{ mt: 2 }}
      >
        <form class="row">
          <div className="col-md-12">
            <div className="form-group">{textContent}</div>
          </div>

          {/* <div className="btn-lest mt-3">
            <button type="reset" className="btn btn-danger-rgba">
              <DoNotDisturbAltIcon color="#fff" /> Reset
            </button>
            {loadingBTN ? (
              <button type="text" disabled className="btn btn-primary-rgba">
                <CheckCircleIcon color="#fff" /> Loading...
              </button>
            ) : (
              <button type="submit" className="btn btn-primary-rgba">
                <CheckCircleIcon color="#fff" /> Craete
              </button>
            )}
            </div>*/}
        </form>
      </Typography>
    </Fragment>
  );
}

export default PopUpAppointment;
