import React from "react";
import "./DeletePopUpAppointment.scss";
import Typography from "@mui/material/Typography";
import { useDispatch } from "react-redux";
import { deleteappointment } from "../../../store/appointmentSlice";

function DeletePopUpAppointment({ data, handleClose, type, selectedRowIds }) {
  let dispatch = useDispatch();
  let dataobj = new FormData();
  dataobj.append("_method", "DELETE");
  let onsubmitfn = (e) => {
    e.preventDefault();
    if (type === "all") {
      dispatch(deleteappointment({ id: selectedRowIds, data: dataobj }));
    } else {
      dispatch(deleteappointment({ id: data.id, data: dataobj }));
    }
  };

  return (
    <Typography id="transition-modal-description" sx={{ mt: 2 }}>
      {type === "all" ? (
        selectedRowIds.length <= 0 ? (
          <div
            className="p-2"
            style={{ color: "red", fontSize: "22px", fontWeight: 700 }}
          >
            There Are No Rows Seleceted
          </div>
        ) : (
          <form className="waning" onSubmit={(e) => onsubmitfn(e)}>
            <h2>Are You Sure ?</h2>

            <p className="message">
              Do you really want to delete All Selected? This process cannot be
              undone.
            </p>

            <div className="btnleist-delte">
              <span type="text" className="btn btn-n" onClick={handleClose}>
                no
              </span>
              <button
                className="btn btn-y"
                onClick={() => {
                  handleClose();
                }}
              >
                yes
              </button>
            </div>
          </form>
        )
      ) : (
        <form className="waning" onSubmit={(e) => onsubmitfn(e)}>
          <h2>Are You Sure ?</h2>

          <p className="message">
            Do you really want to delete {data.row.first_name}? This process
            cannot be undone.
          </p>

          <div className="btnleist-delte">
            <span type="text" className="btn btn-n" onClick={handleClose}>
              no
            </span>
            <button
              className="btn btn-y"
              onClick={() => {
                handleClose();
              }}
            >
              yes
            </button>
          </div>
        </form>
      )}
    </Typography>
  );
}

export default DeletePopUpAppointment;
