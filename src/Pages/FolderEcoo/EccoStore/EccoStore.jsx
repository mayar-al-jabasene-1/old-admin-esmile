import React from "react";
import { Link } from "react-router-dom";
import ErrorIcon from "../../../assists/coming-soon.svg"; // Import the SVG file

function EccoStore() {
  return (
    <div className="error-page">
      <img src={ErrorIcon} alt="Error Icon" />
      <p>Coming Soon</p>
      <Link className="btn btn-primary" to="/">
        Go To Home
      </Link>
    </div>
  );
}

export default EccoStore;
