import React, { Fragment } from "react";
import "./PopUpUser.scss";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { BASE_URL } from "../../../apiConfig";
import EmailIcon from "@mui/icons-material/Email";
import PersonIcon from "@mui/icons-material/Person";
import PhoneEnabledIcon from "@mui/icons-material/PhoneEnabled";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import AdminPanelSettingsIcon from "@mui/icons-material/AdminPanelSettings";
import CakeIcon from "@mui/icons-material/Cake";
import CalendarMonthIcon from "@mui/icons-material/CalendarMonth";
import PublicIcon from "@mui/icons-material/Public";

function PopUpUser({ type, data, handleClose }) {
  let newdata = data.row;

  return (
    <Fragment>
      <Typography id="transition-modal-title" variant="h6" component="h2">
        <div className="head">
          <div className="title-head">
            <span>Information</span>
            <CloseIcon className="icon" onClick={handleClose} />
          </div>
        </div>
      </Typography>

      <Typography
        id="transition-modal-description"
        className="cate-modal"
        sx={{ mt: 2 }}
      >
        <div className="information">
          <div class="row ">
            <div className="col-md-12">
              <div className="box-img text-center">
                {newdata.doctor_picture ? (
                  <img
                    src={`${BASE_URL}/storage/${newdata.doctor_picture}`}
                    alt="..."
                  />
                ) : (
                  <div className="boxImageChar">
                    <span>{newdata.first_name.slice(0, 1).toUpperCase()}</span>
                  </div>
                )}
              </div>
              <h5>{newdata.first_name + " " + newdata.last_name}</h5>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <EmailIcon className="icon" />
                </span>
                <label>: {newdata.email}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <PersonIcon className="icon" />
                </span>
                <label>: {newdata.gender}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <PhoneEnabledIcon className="icon" />
                </span>
                <label>: {newdata.phone_number}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <LocationOnIcon className="icon" />
                </span>
                <label>: {newdata.location}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <LocationOnIcon className="icon" />
                </span>
                <label>: {newdata.location_details}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <PublicIcon className="icon" />
                </span>
                <label>: {newdata.clinic_knowledge}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <CakeIcon className="icon" />
                </span>
                <label>: {newdata.birthday}</label>
              </div>
            </div>
            <div className="col-md-12">
              <div className="deatils-box">
                <span>
                  <AdminPanelSettingsIcon className="icon" />
                </span>
                <label>: {newdata.sickness}</label>
              </div>
            </div>
          </div>
        </div>
      </Typography>
    </Fragment>
  );
}

export default PopUpUser;
