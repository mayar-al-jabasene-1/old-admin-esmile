import React, { useEffect, useState } from "react";
import "./MainUser.scss";
import Sidebar from "../../../components/Sidebar/Sidebar";
import Navbar from "../../../components/Navbar/Navbar";
import Datatable from "../../../components/datatable/Datatable";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getusers } from "../../../store/userslice";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import { BASE_URL, BASE_URL_img } from "../../../apiConfig";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import ModalDelete from "../../../components/ModalDelete/ModalDelete";
import ModalMulti from "../../../components/ModalMulti/ModalMulti";
import * as XLSX from "xlsx";
import jsPDF from "jspdf"; // Import the jsPDF library
import "jspdf-autotable"; // Import the jspdf-autotable plugin
import SettingsIcon from "@mui/icons-material/Settings";
import html2canvas from "html2canvas";

function MainUser() {
  // State variable to store selected row IDs
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
    console.log("Selected Row IDs:", selectionModel);
  };

  console.log("selectedRowIds==>", selectedRowIds);

  let dispatch = useDispatch();

  useEffect(() => {
    dispatch(getusers());
  }, []);

  let { data, loading, error } = useSelector((state) => state.users);
  let navigation = useNavigate();

  let gotonewfn = (id, name) => {
    if (name === "add") {
      navigation("/users/add");
    }
    if (name === "edit") {
      navigation(`/users/edit/${id}`);
    }
    if (name === "show") {
      navigation(`/users/${id}`);
    }
  };

  const userColumns = [
    { field: "id", headerName: "ID", width: 60, sortable: "desc" },
    {
      field: "first_name",
      headerName: "Patient",
      width: 230,
      renderCell: (params) => {
        return (
          <div className="cellWithImg">
            {params.row.user_picture ? (
              <img
                className="cellImg"
                src={`${BASE_URL}/storage/${params.row.user_picture}`}
                alt="avatar"
              />
            ) : (
              <div className="boxImageChar">
                <span> {params.row.first_name.slice(0, 1)}</span>
              </div>
            )}

            {params.row.first_name + " " + params.row.last_name}
          </div>
        );
      },
    },
    {
      field: "gender",
      headerName: "Gender",
      sortable: false,
      width: 90,
    },
    {
      field: "phone_number",
      headerName: "Phone Number",
      sortable: false,
      width: 150,
    },
    {
      field: "location",
      headerName: "Address",
      sortable: false,
      width: 130,
    },
    {
      field: "clinic_knowledge",
      headerName: "Heard About Us from",
      sortable: false,
      width: 160,
    },
    {
      field: "birthday",
      headerName: "Date Of Birth",
      sortable: false,
      width: 120,
      renderCell: (params) => {
        return (
          <div className="remaining">{params.row.birthday.split(" ")[0]}</div>
        );
      },
    },
    {
      field: "Action",
      headerName: "Action",
      sortable: false,
      width: 180,
      renderCell: (params) => {
        return (
          <div className="box-action d-flex gap-3">
            <ModalMulti params={params} text="Users" type="viwe" />
            <EditIcon
              onClick={(e) => gotonewfn(params.id, "edit")}
              className="icon-edit"
            />
            <ModalDelete text="user" params={params} />
          </div>
        );
      },
    },
  ];

  let [togglePrint, setTogglePrint] = useState(false);
  const [isCopied, setIsCopied] = useState(false);

  // Add a function to handle XLSX export

  const handleExportXLSX = () => {
    const dataForExport = data.map((row) => {
      // Create a copy of the row without "user_picture" and "doctor_picture" columns
      const { user_picture, doctor_picture, ...rowData } = row;
      return rowData;
    });

    const wb = XLSX.utils.book_new();
    const ws = XLSX.utils.json_to_sheet(dataForExport);
    XLSX.utils.book_append_sheet(wb, ws, "Patiend Data");
    XLSX.writeFile(wb, "Patiend_data.xlsx");
  };

  // Add a function to handle CSV export
  const handleExportCSV = () => {
    const csvContent =
      "data:text/csv;charset=utf-8," + encodeURI(generateCSV());

    const link = document.createElement("a");
    link.setAttribute("href", csvContent);
    link.setAttribute("download", "Patiend_data.csv");
    link.click();
  };

  // Function to generate CSV data
  const generateCSV = () => {
    const columnsToExport = Object.keys(data[0]).filter(
      (col) => col !== "user_picture" && col !== "doctor_picture"
    );

    // Generate header row
    let csvData = columnsToExport.join(",") + "\n";

    // Generate data rows
    data.forEach((row) => {
      const rowData = columnsToExport.map((col) => row[col]).join(",");
      csvData += rowData + "\n";
    });

    return csvData;
  };

  // Add a function to handle PDF export
  const handleExportPDF = () => {
    // Create a copy of the data with the "last_name" field and without the "Action" column
    const dataForPDF = data.map((row) => {
      const { Action, ...rowData } = row;
      rowData.last_name = row.last_name;
      return rowData;
    });

    const doc = new jsPDF();
    doc.text("Patient Data", 10, 10);

    // Define the columns directly without accessing userColumns
    const columns = [
      "id",
      "first_name",
      "last_name",
      "gender",
      "phone_number",
      "location",
      "location_details",
      "birthday",
    ];

    const rows = dataForPDF.map((row) => columns.map((col) => row[col]));

    // Create headers for the columns
    const columnHeaders = columns.map((col) => {
      const columnConfig = userColumns.find((column) => column.field === col);
      return columnConfig ? columnConfig.headerName : col;
    });

    doc.autoTable({
      head: [columnHeaders],
      body: rows,
    });

    doc.save("Patient_data.pdf");
  };
  // Add a function to handle copying data to clipboard
  const handleCopyToClipboard = () => {
    const columns = Object.keys(data[0]);
    const rows = data.map((row) => columns.map((col) => row[col]));

    const copyText = rows.map((row) => row.join("\t")).join("\n");

    navigator.clipboard
      .writeText(copyText)
      .then(() => {
        console.log("Data copied to clipboard");
        setIsCopied(true); // Set the state to show the success message
        setTimeout(() => {
          setIsCopied(false); // Hide the success message after a delay
        }, 3000); // Hide after 3 seconds
      })
      .catch((error) => {
        console.error("Failed to copy data:", error);
      });
  };

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="two-box">
          <div className="users">
            <div className="header-top">
              <h3>Patients</h3>

              <div className="btn-list">
                <div
                  onClick={() => setTogglePrint(!togglePrint)}
                  className="icon-list-print me-2"
                >
                  <SettingsIcon />
                  <div className={togglePrint ? "show-print" : "d-none"}>
                    <span onClick={handleExportXLSX}>Excel</span>
                    <span onClick={handleExportCSV}>CSV</span>
                    <span onClick={handleCopyToClipboard}>Copy</span>
                    <span onClick={handleExportPDF}>PDF</span>
                  </div>
                </div>
                {isCopied && (
                  <div id="datatables_buttons_info" className="dt-button-info">
                    <h2>Copy to clipboard</h2>
                    <div>
                      Copied {Object.keys(data[0]).length} rows to clipboard
                    </div>
                  </div>
                )}

                <button
                  className="btn btn-add"
                  onClick={(e) => gotonewfn("", "add")}
                >
                  Add New
                </button>
                <button className="btn btn-delete">
                  <ModalDelete
                    text="user"
                    params={data}
                    type="all"
                    selectedRowIds={selectedRowIds}
                  />
                </button>
              </div>
            </div>
            <div className="data-table">
              {loading ? (
                <div className="loading">
                  <CircularIndeterminate />
                </div>
              ) : (
                <Datatable
                  userColumns={userColumns}
                  userRows={data && data}
                  onSelectionModelChange={handleSelectionChange}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainUser;
