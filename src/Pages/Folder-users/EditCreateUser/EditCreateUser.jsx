import React, { useEffect, useState } from "react";
import "./EditCreateUser.scss";
import Sidebar from "../../../components/Sidebar/Sidebar";
import Navbar from "../../../components/Navbar/Navbar";
import {
  createuser,
  getsingeuser,
  resetDataCreateAction,
  resetDataUpdateAction,
  updateuser,
} from "../../../store/userslice";
import { useDispatch, useSelector } from "react-redux";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import { useLocation, useNavigate, useParams } from "react-router-dom";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import { BASE_URL } from "../../../apiConfig";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";
import ErrorPage from "../../ErrorPage/ErrorPage";

function EditCreateUser() {
  let dispatch = useDispatch();
  let { name, id } = useParams();
  let {
    loading,
    datasingle,
    laodingBTN,
    dataCreate,
    dataupdate,
    errorSingle,
    loadingSingle,
  } = useSelector((state) => state.users);

  useEffect(() => {
    if (name === "edit") {
      dispatch(getsingeuser(id));
    }
  }, []);

  useEffect(() => {
    if (name === "edit" && datasingle[0]) {
      setinp({
        first_name: datasingle[0].first_name,
        last_name: datasingle[0].last_name,
        gender: datasingle[0].gender,
        phone_number: datasingle[0].phone_number,
        location: datasingle[0].location,
        location_details: datasingle[0].location_details,
        password: "123@12mmnnMkl",
        birthday: datasingle[0].birthday,
        clinic_knowledge: datasingle[0].clinic_knowledge,
        sensitive: datasingle[0].sensitive,
        sickness: datasingle[0].sickness,
        clinic_note: datasingle[0].clinic_note,
        sensitive_note: datasingle[0].sensitive_note,
      });
      setfile(datasingle[0].user_picture && datasingle[0].user_picture);
    }
  }, [datasingle]);

  const path = useNavigate();

  useEffect(() => {
    if (dataCreate || dataupdate) {
      dispatch(resetDataCreateAction(false));
      dispatch(resetDataUpdateAction(false));
      path("/users");
    }
  }, [dataCreate, dataupdate]);

  let [file, setfile] = useState(null);
  let [inp, setinp] = useState({
    first_name: "",
    last_name: "",
    gender: "",
    phone_number: "",
    location: "",
    location_details: "",
    password: "123@12mmnnMkl",
    birthday: "",
    clinic_knowledge: "",
    sensitive: "",
    sickness: "",
    clinic_note: "",
    sensitive_note: "",
  });

  let getdata = (e) => {
    setinp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let data = new FormData();
  file instanceof File && data.append("user_picture", file);
  data.append("first_name", inp.first_name);
  data.append("last_name", inp.last_name);
  data.append("gender", inp.gender);
  data.append("phone_number", inp.phone_number);

  data.append("location", inp.location);
  data.append("clinic_knowledge", inp.clinic_knowledge);
  data.append("sickness", inp.sickness);
  data.append("sensitive", inp.sensitive);
  data.append("location_details", inp.location_details);
  data.append("birthday", inp.birthday);
  data.append("password", "123%Mm123123");
  data.append("clinic_note", inp.clinic_note);
  data.append("sensitive_note", inp.sensitive_note);

  let [focusfirstname, setfocusfirstname] = useState(false);
  let [focuslastname, setfocuslastname] = useState(false);
  let [focusphone, setfocusphone] = useState(false);
  let [focusgender, setfocusgender] = useState(false);
  let [focuslocation, setfocuslocation] = useState(false);
  let [focuslocationdetails, setfocuslocationdetails] = useState(false);
  let [focussickness, setfocussickness] = useState(false);
  let [focusnotecli, setfocusnotecli] = useState(false);
  let [focussensitivenote, setfocussensitivenote] = useState(false);
  let bluefn = (e) => {
    if (e === "first_name") {
      setfocusfirstname(true);
    }
    if (e === "last_name") {
      setfocuslastname(true);
    }
    if (e === "gender") {
      setfocusgender(true);
    }
    if (e === "phone_number") {
      setfocusphone(true);
    }
    if (e === "location") {
      setfocuslocation(true);
    }
    if (e === "location_details") {
      setfocuslocationdetails(true);
    }
    if (e === "sickness") {
      setfocussickness(true);
    }
    if (e === "sensitive_note") {
      setfocussensitivenote(true);
    }
    if (e === "clinic_note") {
      setfocusnotecli(true);
    }
  };

  let sun = (e) => {
    e.preventDefault();
    if (name === "add") {
      data.append(
        "email",
        inp.first_name + (Math.random() * 100).toFixed(0) + "22@hotmail.com"
      );
      if (inp.gender.length > 0 && inp.location.length > 0) {
        dispatch(createuser(data));
        // path("/users");
      }
    } else {
      if (inp.gender.length > 0 && inp.location.length > 0) {
        data.append("_method", "PUT");
        dispatch(updateuser({ data: data, id: id }));
      }
    }
  };

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="new-user">
          <h2 className="title">
            {name === "add" ? "Add New Patient" : "Edit Patient"}
          </h2>

          {errorSingle ? (
            <ErrorPage />
          ) : loadingSingle ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <form className="Details " onSubmit={(e) => sun(e)}>
              <div className="one ">
                <p className="pp">Patient Details:</p>

                <div className="person-details row">
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label htmlFor="file">
                        <img
                          className="b6e5"
                          src={
                            file instanceof File
                              ? URL.createObjectURL(file)
                              : file
                              ? `${BASE_URL}/storage/${file}`
                              : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                          }
                          alt="..."
                        />
                        <CloudUploadIcon className="icon ms-2" />
                      </label>
                      <input
                        type="file"
                        id="file"
                        name="up"
                        placeholder="name"
                        style={{ display: "none" }}
                        onChange={(e) => {
                          const selectedFile = e.target.files[0];
                          if (selectedFile) {
                            // Set the selected file directly as user_picture
                            setfile(selectedFile);
                          } else {
                            // If no file selected, set user_picture to the URL from the server
                            setfile(
                              `${BASE_URL}/storage/${datasingle.user_picture}`
                            );
                          }
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        First Name <small>*</small> :{" "}
                      </label>
                      <input
                        name="first_name"
                        type="text"
                        autoComplete="off"
                        placeholder="Enter your FirstName.."
                        required
                        value={inp.first_name}
                        pattern="^[A-Za-z0-9ا-ي ]{3,16}$"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("first_name");
                        }}
                        focused={focusfirstname.toString()}
                      />
                      <span>
                        FirstName should be 3-16 characters and shouldn't
                        include any special characters
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Last Name <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.last_name}
                        pattern="^[A-Za-z0-9ا-ي أئ]{3,16}$"
                        name="last_name"
                        placeholder="Enter Your LastName"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("last_name");
                        }}
                        focused={focuslastname.toString()}
                      />
                      <span>
                        LastName should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Gender <small>*</small> :{" "}
                      </label>
                      <select
                        style={{ width: "70%" }}
                        className="form-select"
                        name="gender"
                        onChange={(e) => getdata(e)}
                        required
                        onBlur={() => {
                          bluefn("gender");
                        }}
                        focused={focusgender.toString()}
                        defaultValue={
                          name === "edit" ? inp.gender : "Chosse one please"
                        }
                      >
                        <option
                          disabled
                          value={
                            name === "edit" ? inp.gender : "Chosse one please"
                          }
                        >
                          {name === "edit" ? inp.gender : "Chosse one please"}
                        </option>
                        <option value={"male"}>male</option>
                        <option value={"female"}>female</option>
                      </select>
                      <span>Please Pick a Gender</span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Phone Number <small>*</small> :{" "}
                      </label>
                      <input
                        type="number"
                        required
                        pattern="^\d{7,10}$"
                        name="phone_number"
                        value={inp.phone_number}
                        placeholder="press {7->10} numbers"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("phone_number");
                        }}
                        focused={focusphone.toString()}
                      />
                      <span>
                        You must add 7 digits as a minimum or 10 digits as a
                        maximum
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Allergies <small>*</small> :{" "}
                      </label>
                      <select
                        style={{ width: "70%" }}
                        className="form-select"
                        name="sensitive"
                        onChange={(e) => getdata(e)}
                        required
                        defaultValue={
                          name === "edit" ? inp.sensitive : "Chosse one please"
                        } // Use defaultValue prop
                      >
                        <option
                          disabled
                          value={
                            name === "edit"
                              ? inp.sensitive
                              : "Chosse one please"
                          }
                        >
                          {name === "edit"
                            ? inp.sensitive
                            : "Chosse one please"}
                        </option>
                        <option value={"yes"}>yes</option>
                        <option value={"no"}>no</option>
                        <option value={"maybe"}>maybe</option>
                        <option value={"dont_know"}>dont know</option>
                      </select>
                    </div>
                  </div>
                  {inp.sensitive !== "no" && inp.sensitive !== "" && (
                    <div className="col-lg-4 col-sm-6">
                      <div className="det-inp">
                        <label>
                          Allergies note <small>*</small> :{" "}
                        </label>
                        <input
                          type="text"
                          required
                          name="sensitive_note"
                          value={inp.sensitive_note}
                          placeholder="sensitive note"
                          onChange={(e) => {
                            getdata(e);
                          }}
                          onBlur={() => {
                            bluefn("sensitive_note");
                          }}
                          focused={focussensitivenote.toString()}
                        />
                        <span>
                          sensitive note Details should be 3-16 characters and
                          shouldn't include any special characters
                        </span>
                      </div>
                    </div>
                  )}

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        How Did You Hear About Us <small>*</small> :{" "}
                      </label>
                      <select
                        style={{ width: "70%" }}
                        className="form-select"
                        name="clinic_knowledge"
                        onChange={(e) => getdata(e)}
                        required
                        defaultValue={
                          name === "edit"
                            ? inp.clinic_knowledge
                            : "Chosse one please"
                        } // Use defaultValue prop
                      >
                        <option
                          disabled
                          value={
                            name === "edit"
                              ? inp.clinic_knowledge
                              : "Chosse one please"
                          }
                        >
                          {name === "edit"
                            ? inp.clinic_knowledge
                            : "Chosse one please"}
                        </option>
                        <option value={"social_media"}>social media</option>
                        <option value={"through_someone"}>
                          through someone
                        </option>
                        <option value={"road_sign"}>road sign</option>
                        <option value={"recommendation"}>recommendation</option>
                        <option value={"etc"}>etc</option>
                      </select>
                    </div>
                  </div>
                  {inp.clinic_knowledge === "etc" && (
                    <div className="col-lg-4 col-sm-6">
                      <div className="det-inp">
                        <label>
                          Here Us From? <small>*</small> :{" "}
                        </label>
                        <input
                          type="text"
                          required
                          name="clinic_note"
                          value={inp.clinic_note}
                          placeholder="clinic note"
                          onChange={(e) => {
                            getdata(e);
                          }}
                          onBlur={() => {
                            bluefn("clinic_note");
                          }}
                          focused={focusnotecli.toString()}
                        />
                        <span>
                          clinic_note Details should be 3-16 characters and
                          shouldn't include any special characters
                        </span>
                      </div>
                    </div>
                  )}

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Address (1) <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.location}
                        name="location"
                        pattern="^[A-Za-z0-9ا-ي]{3,16}$"
                        placeholder="Dubai"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("location");
                        }}
                        focused={focuslocation.toString()}
                      />
                      <span>
                        Address should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Diseases <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        name="sickness"
                        value={inp.sickness}
                        placeholder="hinneens"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("sickness");
                        }}
                        focused={focussickness.toString()}
                      />
                      <span>
                        Diseases should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Address (2) <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.location_details}
                        name="location_details"
                        placeholder="Rukn al den"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("location_details");
                        }}
                        focused={focuslocationdetails.toString()}
                      />
                      <span>
                        Address should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Date Of Birth <small>*</small> :{" "}
                      </label>
                      <input
                        type="date"
                        required
                        value={inp.birthday.split(" ")[0]}
                        name="birthday"
                        style={{ color: "gray" }}
                        onChange={(e) => {
                          getdata(e);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>

              {laodingBTN ? (
                <button
                  disabled
                  className="submit btn view"
                  style={{
                    cursor: "no-drop",
                  }}
                >
                  loading...
                </button>
              ) : name === "add" ? (
                <button className="submit btn view">Submit</button>
              ) : (
                <button className="submit btn view">Update</button>
              )}
            </form>
          )}
        </div>
      </div>
    </div>
  );
}

export default EditCreateUser;
