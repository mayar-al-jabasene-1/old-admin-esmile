import React, { useEffect, useState } from "react";
import "./RoleAndPermision.scss";
import Navbar from "../../../components/Navbar/Navbar";
import Datatable from "../../../components/datatable/Datatable";
import { Link, useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import DeletePopUpRole from "../DeletePopUpRole/DeletePopUpRole";
import ModalDelete from "../../../components/ModalDelete/ModalDelete";
import AddIcon from "@mui/icons-material/Add";
// import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import ErrorCompo from "../../../components/ErrorCompo/ErrorCompo";

import { useDispatch, useSelector } from "react-redux";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import Sidebar from "../../../components/Sidebar/Sidebar";

function RoleAndPermision() {
  let navigation = useNavigate();
  const [t, i18n] = useTranslation();

  // let dispatch = useDispatch();
  // useEffect(() => {
  //   dispatch(getAllRoles());
  // }, []);

  // State variable to store selected row IDs
  const [selectedRowIds, setSelectedRowIds] = useState([]);

  // Function to handle selection change
  const handleSelectionChange = (selectionModel) => {
    // Store the selected row IDs in state
    setSelectedRowIds(selectionModel);
    // Log the selected row IDs to the console
    console.log("Selected Row IDs:", selectionModel);
  };

  let roles = [
    {
      id: 1,
      name: "admin",
    },
    {
      id: 2,
      name: "instructor",
    },
    {
      id: 3,
      name: "user",
    },
    {
      id: 5,
      name: "company manager",
    },
    {
      id: 6,
      name: "bank manager",
    },
    {
      id: 43,
      name: "uty",
    },
  ];

  // let { data: roles, loading, error } = useSelector((state) => state.roles);

  const userColumns = [
    { field: "id", headerName: t("ID"), width: 150 },
    {
      field: "name",
      headerName: t("Name"),
      width: 750,
      renderCell: (params) => {
        return <div className="cellWithImg">{`${params.row.name}`}</div>;
      },
    },
    {
      field: "Action",
      headerName: t("Action"),
      sortable: false,
      filter: false,
      width: 130,
      renderCell: (params) => {
        return <div></div>;
      },
    },
  ];

  // let { dataPermP, loadingPermP, errorPermP } = useSelector(
  //   (state) => state.roles
  // );

  // let Create = checkpermation([{ name: "role.create" }], dataPermP);
  // let Delete = checkpermation([{ name: "role.delete" }], dataPermP);
  let mm = false;
  let error = false;
  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="two-box">
          <div className="users">
            <div className="header-top">
              <h3>{t("Roles")}</h3>
              <div className="btn-list">
                <Link to={`/roles-permision/add`} className="btn  btn-add">
                  <AddIcon /> {t("Add New")}
                </Link>
                <button className="btn btn-delete">
                  <ModalDelete
                    filter="regions"
                    params={roles}
                    type="all"
                    selectedRowIds={selectedRowIds}
                  />
                </button>
              </div>
            </div>
          </div>
          <div className="table">
            {error ? (
              <ErrorCompo />
            ) : mm ? (
              <div className="loading">
                <CircularIndeterminate />
              </div>
            ) : (
              <Datatable
                userColumns={userColumns}
                userRows={roles ? roles : []}
                onSelectionModelChange={handleSelectionChange}
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default RoleAndPermision;
