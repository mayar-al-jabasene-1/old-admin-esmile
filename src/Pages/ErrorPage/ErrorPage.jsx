import React from "react";
import "./ErrorPage.scss";
import ErrorIcon from "../../assists/error-icon.svg"; // Import the SVG file

function ErrorPage() {
  return (
    <div className="error-page">
      <img src={ErrorIcon} alt="Error Icon" />
      <p>Something Went Wrong</p>
      <button
        className="btn btn-primary"
        onClick={() => window.location.reload()}
      >
        Refresh
      </button>
    </div>
  );
}

export default ErrorPage;
