import React, { Fragment, useEffect, useState } from "react";
import "./EditCreateDoctros.scss";
import { BASE_URL } from "../../../apiConfig";
import CircularIndeterminate from "../../../components/CircularIndeterminate/CircularIndeterminate";
import Sidebar from "../../../components/Sidebar/Sidebar";
import Navbar from "../../../components/Navbar/Navbar";
import { createuser, getsingeuser, updateuser } from "../../../store/userslice";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import RemoveRedEyeIcon from "@mui/icons-material/RemoveRedEye";
import {
  getDoctorById,
  registerDoctor,
  resetDataCreateAction,
  resetDataUpdateAction,
  updateDoctor,
} from "../../../store/DoctorSlice";
import AvilableTime from "../../../components/AvilableTime/AvilableTime";
import AddIcon from "@mui/icons-material/Add";
import { notifyError } from "../../../Notification";
import ErrorPage from "../../ErrorPage/ErrorPage";

function EditCreateDoctros() {
  // Initialize number based on the number of available days
  let availableDays = [
    "saturday",
    "sunday",
    "monday",
    "tuesday",
    "wednesday",
    "thursday",
    "friday",
  ];

  const [selectedDays, setSelectedDays] = useState([]);

  const handleDaySelection = (selectedDay, index) => {
    setSelectedDays((prevSelectedDays) => {
      // Find the index of the selectedDay in the array
      const existingIndex = prevSelectedDays.findIndex(
        (day) => day.index === index
      );

      // If the selectedDay already exists in the array, update its value
      if (existingIndex !== -1) {
        const updatedSelectedDays = [...prevSelectedDays];
        updatedSelectedDays[existingIndex] = { day: selectedDay, index };
        return updatedSelectedDays;
      } else {
        // If the selectedDay is not in the array, add it
        return [...prevSelectedDays, { day: selectedDay, index }];
      }
    });
  };

  // When the "Show more" icon is clicked, increment the length of the number array

  let dispatch = useDispatch();
  let { name, id } = useParams();

  let {
    dataSingle: DoctorSingle,
    loading: loadingD,
    laodingSingle,
    dataCreate,
    dataUpdate,
    errorSingle,
    laodingBTN,
  } = useSelector((state) => state.doctors);

  useEffect(() => {
    if (name === "edit") {
      dispatch(getDoctorById(id));
    }
  }, []);

  useEffect(() => {
    if (dataCreate || dataUpdate) {
      dispatch(resetDataCreateAction(false));
      dispatch(resetDataUpdateAction(false));
      path("/doctors");
    }
  }, [dataCreate, dataUpdate]);

  let [showPass, setShowPass] = useState(false);
  let [number, setNumber] = useState(name === "edit" ? "" : [1]);
  useEffect(() => {
    if (name === "edit" && DoctorSingle[0]) {
      setNumber(DoctorSingle[0].availability);
      setDataAppo(DoctorSingle[0].availability);
      setinp({
        first_name: DoctorSingle[0].first_name,
        last_name: DoctorSingle[0].last_name,
        email: DoctorSingle[0].email,
        gender: DoctorSingle[0].gender,
        availability_type: DoctorSingle[0].availability_type,
        phone_number: DoctorSingle[0].phone_number,
        location: DoctorSingle[0].location,
        location_details: DoctorSingle[0].location_details,
        birthday: DoctorSingle[0].birthday,
        competence_type: DoctorSingle[0].competence_type,
        years_of_experience: DoctorSingle[0].years_of_experience,
      });
      setfile(DoctorSingle[0].doctor_picture && DoctorSingle[0].doctor_picture);
      // Create an array of selected days based on DoctorSingle[0].availability
      const selectedDaysFromDoctor = DoctorSingle[0].availability.map(
        (avail, index) => {
          return { day: avail.day_of_week, index };
        }
      );

      // Set the selectedDays state with the selected days from the doctor's data
      setSelectedDays(selectedDaysFromDoctor);
    }
  }, [DoctorSingle]);

  const path = useNavigate();
  let [file, setfile] = useState(null);
  let [inp, setinp] = useState({
    first_name: "",
    email: "",
    last_name: "",
    gender: "",
    availability_type: "",
    phone_number: "",
    location: "",
    location_details: "",
    password: "",
    birthday: "",
    competence_type: "",
    years_of_experience: "",
  });

  let getdata = (e) => {
    setinp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let [DataAppo, setDataAppo] = useState([{}]);

  let getdataAppo = (e, type, index) => {
    setDataAppo((prev) => {
      const newDataAppo = [...prev];
      let currentItem = newDataAppo[index] || {};

      if (type === "start") {
        currentItem = { ...currentItem, start_time: e };
      } else if (type === "end") {
        currentItem = { ...currentItem, end_time: e };
      } else {
        currentItem = { ...currentItem, [e.target.name]: e.target.value };
        handleDaySelection(e.target.value, index);
      }

      newDataAppo[index] = currentItem;
      return newDataAppo;
    });
  };

  let data = new FormData();
  file instanceof File && data.append("doctor_picture", file);
  data.append("first_name", inp.first_name);
  data.append("last_name", inp.last_name);
  data.append("gender", inp.gender);
  data.append("availability_type", inp.availability_type);
  data.append("phone_number", inp.phone_number);
  data.append("years_of_experience", inp.years_of_experience);
  data.append("location", inp.location);
  data.append("competence_type", inp.competence_type);
  data.append("location_details", inp.location_details);
  data.append("birthday", inp.birthday);
  data.append("birthday", inp.birthday);
  inp.password && data.append("password", inp.password);
  inp.email && data.append("email", inp.email);

  let [focusfirstname, setfocusfirstname] = useState(false);
  let [focuslastname, setfocuslastname] = useState(false);
  let [focusphone, setfocusphone] = useState(false);
  let [focusgender, setfocusgender] = useState(false);
  let [availability_type, setavailability_type] = useState(false);
  let [focuslocation, setfocuslocation] = useState(false);
  let [focuslocationdetails, setfocuslocationdetails] = useState(false);
  let [focusemail, setfocusemail] = useState(false);
  let [focuspassword, setfocuspassword] = useState(false);
  let [focusYears, setfocusYears] = useState(false);
  let bluefn = (e) => {
    if (e === "first_name") {
      setfocusfirstname(true);
    }
    if (e === "last_name") {
      setfocuslastname(true);
    }
    if (e === "gender") {
      setfocusgender(true);
    }
    if (e === "availability_type") {
      setavailability_type(true);
    }
    if (e === "phone_number") {
      setfocusphone(true);
    }
    if (e === "years_of_experience") {
      setfocusYears(true);
    }
    if (e === "location") {
      setfocuslocation(true);
    }
    if (e === "location_details") {
      setfocuslocationdetails(true);
    }

    if (e === "email") {
      setfocusemail(true);
    }
    if (e === "password") {
      setfocuspassword(true);
      setShowPass(false);
    }
  };

  let sun = (e) => {
    e.preventDefault();
    const reformattedData = {};

    inp.availability_type === "part_time" &&
      DataAppo.forEach((item, index) => {
        if (Object.keys(item).length > 0) {
          const dayPrefix = `days[${index}]`;
          reformattedData[`${dayPrefix}[day_of_week]`] = item.day_of_week;
          reformattedData[`${dayPrefix}[start_time]`] = item.start_time;
          reformattedData[`${dayPrefix}[end_time]`] = item.end_time;
        }
      });

    // Now, add the reformatted data to your FormData object
    for (const key in reformattedData) {
      data.append(key, reformattedData[key]);
    }

    if (name === "add") {
      if (inp.availability_type.length <= 0) {
        notifyError("Please Chosse availability Time ");
      }
      if (inp.gender.length <= 0) {
        notifyError("Please Chosse Gender");
      }
      if (inp.location.length <= 0) {
        notifyError("Please Chosse location");
      }
      if (
        inp.gender.length > 0 &&
        inp.location.length > 0 &&
        inp.availability_type.length > 0
      ) {
        dispatch(registerDoctor(data));
      }
    } else {
      if (
        inp.gender.length > 0 &&
        inp.location.length > 0 &&
        inp.availability_type.length > 0
      ) {
        data.append("_method", "PUT");
        dispatch(updateDoctor({ data: data, id: id }));
      }
    }
  };

  const handleShowMore = () => {
    if (number.length < 7) {
      setNumber([...number, 2]);
    }
  };

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="new-user">
          <h2 className="title">
            {name === "add" ? "Add New Doctor" : "Edit Doctor"}
          </h2>

          {errorSingle ? (
            <ErrorPage />
          ) : laodingSingle ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <form className="Details createDoc" onSubmit={(e) => sun(e)}>
              <div className="one ">
                <p className="pp">Doctor Details :</p>

                <div className=" row">
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label htmlFor="file">
                        <img
                          className="b6e5"
                          src={
                            file instanceof File
                              ? URL.createObjectURL(file)
                              : file
                              ? `${BASE_URL}/storage/${file}`
                              : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                          }
                          alt="..."
                        />
                        <CloudUploadIcon className="icon ms-2" />
                      </label>
                      <input
                        type="file"
                        id="file"
                        name="up"
                        placeholder="name"
                        style={{ display: "none" }}
                        onChange={(e) => {
                          const selectedFile = e.target.files[0];
                          if (selectedFile) {
                            // Set the selected file directly as user_picture
                            setfile(selectedFile);
                          } else {
                            // If no file selected, set user_picture to the URL from the server
                            setfile(
                              `${BASE_URL}/storage/${DoctorSingle[0].doctor_picture}`
                            );
                          }
                        }}
                      />
                    </div>
                  </div>
                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Email <small>*</small> :{" "}
                      </label>
                      <input
                        name="email"
                        type="email"
                        autoComplete="off"
                        placeholder="Enter your Email.."
                        required
                        value={inp.email}
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("email");
                        }}
                        focused={focusemail.toString()}
                      />
                      <span>and @ and without spical characters</span>
                    </div>
                  </div>
                  {name === "add" && (
                    <div className="col-lg-4 col-sm-6">
                      <div className="det-inp pass">
                        <label>
                          Password <small>*</small> :{" "}
                        </label>
                        <input
                          id="5ra"
                          name="password"
                          type={showPass ? "text" : "password"}
                          autoComplete="off"
                          placeholder="Enter your Password.."
                          required
                          value={inp.password}
                          pattern="^(?=.*[0-9].*[0-9].*[0-9].*[0-9].*[0-9].*[0-9])(?=.*[a-zA-Z].*[a-zA-Z]).{6,}$"
                          onChange={(e) => {
                            getdata(e);
                          }}
                          onBlur={() => {
                            bluefn("password");
                          }}
                          onFocus={() => {
                            setShowPass(true);
                          }}
                          focused={focuspassword.toString()}
                        />
                        <span>
                          Password must have at least 6 numbers, 2 characters,
                          and 1 special character
                        </span>
                        {!showPass && (
                          <label htmlFor="5ra">
                            <RemoveRedEyeIcon className="icon-show" />
                          </label>
                        )}
                      </div>
                    </div>
                  )}

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        First Name <small>*</small> :{" "}
                      </label>
                      <input
                        name="first_name"
                        type="text"
                        autoComplete="off"
                        placeholder="Enter your FirstName.."
                        required
                        value={inp.first_name}
                        pattern="^[A-Za-z0-9ا-ي ]{3,16}$"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("first_name");
                        }}
                        focused={focusfirstname.toString()}
                      />
                      <span>
                        FirstName should be 3-16 characters and shouldn't
                        include any special characters
                      </span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Last Name <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.last_name}
                        pattern="^[A-Za-z0-9ا-ي أئ]{3,16}$"
                        name="last_name"
                        placeholder="Enter Your LastName"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("last_name");
                        }}
                        focused={focuslastname.toString()}
                      />
                      <span>
                        LastName should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Gender <small>*</small> :{" "}
                      </label>
                      <select
                        style={{ width: "70%" }}
                        className="form-select"
                        name="gender"
                        onChange={(e) => getdata(e)}
                        required
                        onBlur={() => {
                          bluefn("gender");
                        }}
                        focused={focusgender.toString()}
                        defaultValue={
                          name === "edit" ? inp.gender : "Chosse one please"
                        }
                      >
                        <option
                          disabled
                          value={
                            name === "edit" ? inp.gender : "Chosse one please"
                          }
                        >
                          {name === "edit" ? inp.gender : "Chosse one please"}
                        </option>
                        <option value={"male"}>male</option>
                        <option value={"female"}>female</option>
                      </select>
                      <span>Please Pick a Gender</span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Phone Number <small>*</small> :{" "}
                      </label>
                      <input
                        type="number"
                        required
                        pattern="^\d{7,10}$"
                        maxLength={"10"}
                        name="phone_number"
                        value={inp.phone_number}
                        placeholder="press {7->10} numbers"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("phone_number");
                        }}
                        focused={focusphone.toString()}
                      />
                      <span>
                        You must add 7 digits as a minimum or 10 digits as a
                        maximum
                      </span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        competence type <small>*</small> :{" "}
                      </label>
                      <select
                        style={{ width: "70%" }}
                        className="form-select"
                        name="competence_type"
                        onChange={(e) => getdata(e)}
                        required
                        defaultValue={
                          name === "edit"
                            ? inp.competence_type
                            : "Chosse one please"
                        } // Use defaultValue prop
                      >
                        <option
                          disabled
                          value={
                            name === "edit"
                              ? inp.competence_type
                              : "Chosse one please"
                          }
                        >
                          {name === "edit"
                            ? inp.competence_type
                            : "Chosse one please"}
                        </option>
                        <option value={"general_dentistry"}>
                          general dentistry
                        </option>
                        <option value={"orthodontics"}>orthodontics</option>
                        <option value={"periodontology_and_oral_surgery"}>
                          periodontology & oral surgery
                        </option>
                        <option value={"cosmetic_dentistry"}>
                          cosmetic dentistry
                        </option>
                        <option value={"childrens_dentistry"}>
                          childrens dentistry
                        </option>
                        <option value={"neurology_in_dentistry"}>
                          neurology in dentistry
                        </option>
                      </select>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Address (1) <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.location}
                        name="location"
                        pattern="^[A-Za-z0-9ا-ي]{3,16}$"
                        placeholder="Dubai"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("location");
                        }}
                        focused={focuslocation.toString()}
                      />
                      <span>
                        Address should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        years of experience <small>*</small> :{" "}
                      </label>
                      <input
                        type="number"
                        required
                        pattern="^\d{1,2}$"
                        defaultValue={inp.years_of_experience}
                        name="years_of_experience"
                        placeholder="press {1->2} numbers"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("years_of_experience");
                        }}
                        focused={focusYears.toString()}
                      />
                      <span>You must add your years experience</span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Address (2) <small>*</small> :{" "}
                      </label>
                      <input
                        type="text"
                        required
                        value={inp.location_details}
                        name="location_details"
                        pattern="^[A-Za-z0-9ا-ي]{3,16}$"
                        placeholder="Rukn al den"
                        onChange={(e) => {
                          getdata(e);
                        }}
                        onBlur={() => {
                          bluefn("location_details");
                        }}
                        focused={focuslocationdetails.toString()}
                      />
                      <span>
                        Address should be 3-16 characters and shouldn't include
                        any special characters
                      </span>
                    </div>
                  </div>

                  <div className="col-lg-4 col-sm-6">
                    <div className="det-inp">
                      <label>
                        Date Of Birth <small>*</small> :{" "}
                      </label>
                      <input
                        type="date"
                        className="form-date"
                        required
                        value={inp.birthday.split(" ")[0]}
                        name="birthday"
                        style={{ color: "gray" }}
                        onChange={(e) => {
                          getdata(e);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="one row">
                <p className="pp">Setting :</p>
                <div className="col-lg-12 ">
                  <div className="det-inp">
                    <label>
                      availability Time <small>*</small> :{" "}
                    </label>
                    <select
                      style={{ width: "33.33%" }}
                      className="form-select"
                      name="availability_type"
                      onChange={(e) => getdata(e)}
                      required
                      onBlur={() => {
                        bluefn("availability_type");
                      }}
                      focused={availability_type.toString()}
                      defaultValue={
                        name === "edit"
                          ? inp.availability_type
                          : "Chosse one please"
                      }
                    >
                      <option
                        disabled
                        value={
                          name === "edit"
                            ? inp.availability_type
                            : "Chosse one please"
                        }
                      >
                        {name === "edit"
                          ? inp.availability_type
                          : "Chosse one please"}
                      </option>
                      <option value={"full_time"}>full time</option>
                      <option value={"part_time"}>part time</option>
                    </select>
                    <span>Please Pick a availability Time</span>
                  </div>
                </div>
                <div className="avilable col-lg-12  ">
                  {name === "edit" ? (
                    <Fragment>
                      {number &&
                        number.map((avail, index) => (
                          <AvilableTime
                            key={index}
                            days={availableDays}
                            getdataAppo={getdataAppo}
                            index={index}
                            selectedDays={selectedDays}
                            availability={avail} // Pass the availability data
                            name={name}
                            // Pass the function as a prop
                          />
                        ))}
                      <div className="box-icon" onClick={handleShowMore}>
                        <AddIcon className="icon-add" />
                      </div>
                    </Fragment>
                  ) : (
                    inp.availability_type === "part_time" && (
                      <Fragment>
                        {number &&
                          number.map((avail, index) => (
                            <AvilableTime
                              key={index}
                              days={availableDays}
                              getdataAppo={getdataAppo}
                              index={index}
                              selectedDays={selectedDays}
                              availability={avail} // Pass the availability data
                              name={name}
                              // Pass the function as a prop
                            />
                          ))}
                        <div className="box-icon" onClick={handleShowMore}>
                          <AddIcon className="icon-add" />
                        </div>
                      </Fragment>
                    )
                  )}
                </div>
              </div>
              {laodingBTN ? (
                <button
                  disabled
                  className="submit btn view mt-5"
                  style={{
                    cursor: "no-drop",
                  }}
                >
                  loading...
                </button>
              ) : name === "add" ? (
                <button className="submit btn view mt-5">Submit</button>
              ) : (
                <button className="submit btn view">Update</button>
              )}
            </form>
          )}
        </div>
      </div>
    </div>
  );
}

export default EditCreateDoctros;
