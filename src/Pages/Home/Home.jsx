import "./Home.scss";
import Featured from "../../components/featured/Featured";
import Navbar from "../../components/Navbar/Navbar";
import Recharts from "../../components/recharts/Recharts";
import Sidebar from "../../components/Sidebar/Sidebar";
import Widget from "../../components/Widget/Widget";
import { motion } from "framer-motion";

function Home() {
  //start varants for framer motion
  const widgetvarint = {
    hidden: {
      y: -250,
    },
    visable: {
      y: 0,
    },
  };

  const homevarint = {
    hidden: {
      x: "-30px",
    },
    visable: {
      x: "0",
    },
  };
  const homevarint2 = {
    hidden: {
      x: "+50px",
    },
    visable: {
      x: "0",
    },
  };
  return (
    //Home Page
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10 col-lg-12">
        <motion.div
          className="row"
          variants={widgetvarint}
          initial="hidden"
          animate="visable"
          transition={{ type: "spring", delay: 0.5 }}
        >
          <div className="col-lg-3">
            <Widget type="appointments" />
          </div>
          <div className="col-lg-3">
            <Widget type="users" />
          </div>
          <div className="col-lg-3">
            <Widget type="earning" />
          </div>
          <div className="col-lg-3">
            <Widget type="balance" />
          </div>
        </motion.div>
        <div className="row">
          <motion.div
            className="col-lg-4"
            variants={homevarint}
            initial="hidden"
            animate="visable"
            transition={{
              type: "spring",
              delay: 0.2,
              duration: 2,
            }}
          >
            <Featured />
          </motion.div>
          <motion.div
            className="col-lg-8 rech-home"
            variants={homevarint2}
            initial="hidden"
            animate="visable"
            transition={{ type: "spring", delay: 0.2, duration: 2 }}
          >
            <Recharts aspect={2 / 1} title="Last 6 Months (Revenue)" />
          </motion.div>
        </div>
      </div>
    </div>
  );
}

export default Home;
