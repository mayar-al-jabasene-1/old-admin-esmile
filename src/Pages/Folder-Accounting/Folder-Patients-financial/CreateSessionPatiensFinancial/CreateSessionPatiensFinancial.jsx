import React, { Fragment, useEffect, useRef, useState } from "react";
import "./CreateSessionPatiensFinancial.scss";
import Navbar from "../../../../components/Navbar/Navbar";
import Sidebar from "../../../../components/Sidebar/Sidebar";
import { Editor } from "@tinymce/tinymce-react";
import ComboBox from "../../../../components/AutoComplate/ComboBox";
import CircularIndeterminate from "../../../../components/CircularIndeterminate/CircularIndeterminate";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  createfnaccount,
  getUserFnaccount,
  resetDataCreateAction,
} from "../../../../store/patientsfinancialSlice";
import { getAllDoctors } from "../../../../store/DoctorSlice";
import CloudUploadIcon from "@mui/icons-material/CloudUpload";
import { BASE_URL } from "../../../../apiConfig";
import { notifyError } from "../../../../Notification";
import ErrorCompo from "../../../../components/ErrorCompo/ErrorCompo";
import Tooth from "../../../TestChose/Test";

function CreateSessionPatiensFinancial() {
  const editorRef = useRef(null);
  const titleref = useRef(null);
  let [tooth, setTooth] = useState();
  let [toggle, setToggle] = useState(false);
  console.log("toot==", tooth);
  const handleEditorChange = (content, editor) => {
    setSelectDetails(content);
  };

  let { name } = useParams();

  const [selectname, setSelectName] = useState(null);
  const [selectdoctor, setSelectDoctor] = useState(null);
  const [selectdetails, setSelectDetails] = useState(null);

  let dispatch = useDispatch();
  useEffect(() => {
    dispatch(getUserFnaccount());
    dispatch(getAllDoctors());
  }, []);

  let {
    data,
    loading: loadingUsers,
    error,
  } = useSelector((state) => state.users);
  let {
    data: doctors,
    loading: loadingDoctors,
    error: errorDoctor,
  } = useSelector((state) => state.doctors);

  let { Allpatient, loadingAllpatient, laodingBTN, dataCreate, errorAllFn } =
    useSelector((state) => state.patientsfinancial);

  let [file, setfile] = useState(null);
  let [inp, setinp] = useState({
    full_cost: "",
    paid: "",
    payment_type: "",
  });

  let [focusfullcost, setfocusfullcost] = useState(false);
  let [focuspaid, setfocuspaid] = useState(false);
  let [focusdescription, setfocusdescription] = useState(false);

  const path = useNavigate();

  useEffect(() => {
    if (dataCreate) {
      dispatch(resetDataCreateAction(false));
      path("/Patients-financial");
    }
  }, [dataCreate]);

  let bluefn = (e) => {
    if (e === "full_cost") {
      setfocusfullcost(true);
    }
    if (e === "paid") {
      setfocuspaid(true);
    }
    if (e === "description") {
      setfocusdescription(true);
    }
  };

  let getdata = (e) => {
    setinp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let sendData = (e) => {
    e.preventDefault();
    if (selectname === null) {
      notifyError("Chosse Patient Name please");
    }
    if (selectdoctor === null) {
      notifyError("Chosse Doctor Name please");
    }
    if (selectdetails === null) {
      notifyError("Write Details  please");
    }
    let dataSend = {
      ...inp,
      user_id: selectname && selectname.value,
      doctor_id: selectdoctor && selectdoctor.value,
      description: selectdetails,
      note: selectdetails,
      xray_picture: file,
    };
    if (selectname && selectdoctor && selectdetails) {
      dispatch(createfnaccount({ data: dataSend, id: selectname.value }));
    }
  };

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="new-user newpatient">
          <h2 className="title">
            {name === "add"
              ? "Add New Patient Financial"
              : "Edit Patient Financial"}
          </h2>

          <form className="Details" onSubmit={(e) => sendData(e)}>
            <div className="one">
              <p className="pp">Personal:</p>
            </div>
            <div className="row">
              <div className="col-lg-4 col-sm-6 mb-5">
                <div className="det-inp">
                  <label htmlFor="file">
                    <img
                      className="b6e5"
                      src={
                        file instanceof File
                          ? URL.createObjectURL(file)
                          : file
                          ? `${BASE_URL}/storage/${file}`
                          : "https://icon-library.com/images/no-image-icon/no-image-icon-0.jpg"
                      }
                      alt="..."
                    />
                    <CloudUploadIcon className="icon ms-2" />
                  </label>
                  <input
                    type="file"
                    id="file"
                    name="up"
                    placeholder="name"
                    style={{ display: "none" }}
                    onChange={(e) => {
                      const selectedFile = e.target.files[0];
                      if (selectedFile) {
                        // Set the selected file directly as user_picture
                        setfile(selectedFile);
                      } else {
                        // If no file selected, set user_picture to the URL from the server
                      }
                    }}
                  />
                </div>
              </div>

              <div className="col-lg-4 col-sm-6 mb-5">
                <div className="">
                  {errorAllFn ? (
                    <ErrorCompo />
                  ) : loadingAllpatient ? (
                    <CircularIndeterminate />
                  ) : (
                    <Fragment>
                      <div className="mb-3">
                        <label htmlFor="detail">
                          Patient Name :<sup className="text-danger">*</sup>
                        </label>
                      </div>
                      <ComboBox
                        data={Allpatient && Allpatient}
                        type="patients"
                        dataselected={setSelectName}
                      />
                    </Fragment>
                  )}
                </div>
              </div>

              <div className="col-lg-4 col-sm-6 mb-5">
                <div className="">
                  {errorDoctor ? (
                    <ErrorCompo />
                  ) : loadingDoctors ? (
                    <CircularIndeterminate />
                  ) : (
                    <Fragment>
                      <div className="mb-3">
                        <label htmlFor="detail">
                          Doctor Name :<sup className="text-danger">*</sup>
                        </label>
                      </div>
                      <ComboBox
                        data={doctors && doctors}
                        type="doctors"
                        dataselected={setSelectDoctor}
                      />
                    </Fragment>
                  )}
                </div>
              </div>

              <div className="col-lg-4 col-sm-6 mt-3">
                <div className="det-inp">
                  <label>
                    Full Cost <small>*</small> :{" "}
                  </label>
                  <input
                    type="number"
                    required
                    pattern="^[0-9 ]{2,20}$"
                    name="full_cost"
                    placeholder="Press Full Cost please like 9000"
                    focused={focusfullcost.toString()}
                    onChange={(e) => {
                      getdata(e);
                    }}
                    onBlur={() => {
                      bluefn("full_cost");
                    }}
                  />
                  <span>full cost should be 2-18 number</span>
                </div>
              </div>

              <div className="col-lg-4 col-sm-6 mt-3">
                <div className="det-inp">
                  <label>
                    Paid value : <small>*</small> :{" "}
                  </label>
                  <input
                    type="number"
                    name="paid"
                    pattern="^[0-9 ]{2,20}$"
                    onBlur={() => {
                      bluefn("paid");
                    }}
                    focused={focuspaid.toString()}
                    required
                    placeholder="Press Paid Value please like 9000"
                    onChange={(e) => getdata(e)}
                  />
                  <span>paid value should be 2-18 number</span>
                </div>
              </div>
              <div className="col-lg-4 col-sm-6 mt-3">
                <div className="det-inp">
                  <label>
                    Payment Type : <small>*</small>
                  </label>
                  <select
                    className="form-select"
                    name="payment_type"
                    onChange={(e) => getdata(e)}
                    required
                    defaultValue={"Chosse Payment Type"}
                  >
                    <option disabled value={"Chosse Payment Type"}>
                      Chosse Payment Type
                    </option>
                    <option value={"cash"}>cash</option>
                    <option value={"card"}>card</option>
                  </select>
                  <span>Chosse Payment Type</span>
                </div>
              </div>

              <div className="col-lg-6 col-sm-6 mt-5">
                <div className="det-inp">
                  <label>
                    Diagnosis : <small>*</small> :{" "}
                  </label>
                  <input
                    type="text"
                    name="diagnosis"
                    required
                    placeholder="Enter Diagnosis please"
                    onChange={(e) => getdata(e)}
                  />
                </div>

                <div className="det-inp mt-4">
                  <label className="mt-5">
                    Tooth Number : <small>*</small> :{" "}
                  </label>
                  <input
                    type="number"
                    name="tooth_number"
                    required
                    defaultValue={tooth && tooth}
                    readOnly
                    placeholder="Choose Tooth Number from image "
                    onChange={(e) => getdata(e)}
                  />
                </div>
              </div>

              <div className="col-lg-6 col-sm-6 mt-3">
                <Tooth setTooth={setTooth} setToggle={setToggle} />
              </div>

              <div className="col-lg-12 col-sm-12 mt-4 mb-4">
                <div className="">
                  <label htmlFor="detail">
                    Description :<sup className="text-danger">*</sup>
                  </label>
                </div>

                <div className="col-lg-12">
                  <Editor
                    apiKey="zvo8oa2gmt3hxn4sp8v14uzgoe3u26bbufnedvo08zg35gqa"
                    onInit={(evt, editor) => (editorRef.current = editor)}
                    initialValue=""
                    required
                    init={{
                      height: 500,
                      menubar: true,
                      plugins: [
                        "advlist",
                        "autolink",
                        "lists",
                        "link",
                        "image",
                        "charmap",
                        "preview",
                        "anchor",
                        "searchreplace",
                        "visualblocks",
                        "code",
                        "fullscreen",
                        "insertdatetime",
                        "media",
                        "table",
                        "code",
                        "help",
                        "wordcount",
                      ],
                      toolbar:
                        "undo redo | blocks | " +
                        "bold italic forecolor | alignleft aligncenter " +
                        "alignright alignjustify | bullist numlist outdent indent | " +
                        "removeformat | help",
                      content_style:
                        "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
                    }}
                    onEditorChange={handleEditorChange}
                  />
                </div>
              </div>
            </div>

            {laodingBTN ? (
              <button
                disabled
                className="submit btn view"
                style={{
                  cursor: "no-drop",
                }}
              >
                loading...
              </button>
            ) : name === "add" ? (
              <button className="submit btn view">Submit</button>
            ) : (
              <button className="submit btn view">Update</button>
            )}
          </form>
        </div>
      </div>
    </div>
  );
}

export default CreateSessionPatiensFinancial;
