import React, { Fragment, useEffect, useState } from "react";
import "./EditSessionSubPatiensFinancial.scss";
import Sidebar from "../../../../components/Sidebar/Sidebar";
import Navbar from "../../../../components/Navbar/Navbar";
import CollapsibleTable from "../../../../components/TableDropDown/CollapsibleTable";
import Recharts from "../../../../components/recharts/Recharts";
import EmailIcon from "@mui/icons-material/Email";
import PhoneIcon from "@mui/icons-material/Phone";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import HomeIcon from "@mui/icons-material/Home";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useParams } from "react-router-dom";
import {
  createSession,
  createSubSession,
  deleteSession,
  deleteSubSession,
  getUserSessions,
  updateSubSession,
} from "../../../../store/patientsfinancialSlice";
import { getsingeuser } from "../../../../store/userslice";
import { BASE_URL } from "../../../../apiConfig";
import CircularIndeterminate from "../../../../components/CircularIndeterminate/CircularIndeterminate";
import ComboBox from "../../../../components/AutoComplate/ComboBox";
import { getAllDoctors } from "../../../../store/DoctorSlice";
import { notifyError } from "../../../../Notification";
import Tooth from "../../../TestChose/Test";

function EditSessionSubPatiensFinancial() {
  let [popup, setPopup] = useState(false);
  let [popup2, setPopup2] = useState(false);
  let [popup3, setPopup3] = useState(false);
  let [PaidEdit, setPaidEdit] = useState("");
  let [DescEdit, setDescEdit] = useState("");
  let [IdEdit, setIdEdit] = useState("");
  let [PaymentEdit, setPaymentEdit] = useState("Cash");

  //subsession
  let [subpaid, setsubPaid] = useState("");
  let [subDetails, setsubDetails] = useState("");
  let [paymentsub, setPaymentsub] = useState("");
  let [idsubsession, setIdSubsission] = useState("");

  let { id, user_id } = useParams();

  let [datacreate, setDataCreate] = useState({
    description: "",
    full_cost: "",
    paid: "",
    payment_type: "",
  });
  let getdata = (e) => {
    setDataCreate((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  let dispatch = useDispatch();
  const [selectdoctor, setSelectDoctor] = useState(null);
  useEffect(() => {
    dispatch(getUserSessions(user_id));
    dispatch(getsingeuser(user_id));
    dispatch(getAllDoctors());
  }, []);

  let {
    loading: louser,
    datasingle: datauser,
    error: eruser,
  } = useSelector((state) => state.users);

  let {
    loading,
    loadingBTN,
    dataSetion: dataAccount,
    dataCreate: dataCreatePatient,
    dataUpdate: dataupdatePatient,
    error,
  } = useSelector((state) => state.patientsfinancial);

  let { data: doctors, loading: loadingDoctors } = useSelector(
    (state) => state.doctors
  );

  useEffect(() => {
    if (dataCreatePatient || dataupdatePatient) {
      dispatch(getUserSessions(user_id));
      setPopup(false);
      setPopup2(false);
      setPopup3(false);
    }
  }, [dataCreatePatient, dataupdatePatient]);

  let sendToSesion = (e) => {
    e.preventDefault();
    // let id = dataAccount[0].financial_account_id;
    let id = datauser[0].id;
    let createNewSe = {
      ...datacreate,
      doctor_id: selectdoctor && selectdoctor.value,
    };
    if (!selectdoctor) {
      notifyError("Filed Doctor Is Required");
    }
    if (!datacreate.description) {
      notifyError("Filed description Is Required");
    }
    if (!datacreate.full_cost) {
      notifyError("Filed full cost Is Required");
    }
    if (!datacreate.paid) {
      notifyError("Filed paid Is Required");
    }
    if (!datacreate.payment_type) {
      notifyError("Filed paid Is Required");
    }
    if (
      datacreate.description &&
      datacreate.full_cost &&
      datacreate.paid &&
      datacreate.payment_type
    ) {
      dispatch(createSession({ data: createNewSe, id: id }));
    }
  };

  //deelte session :
  let deletesesionfn = (e) => {
    // setDeleteSession(e)
    dispatch(deleteSession(e));
  };

  //create subsession
  let datasub = {
    payment_type: paymentsub,
    paid: subpaid,
    description: subDetails,
  };

  //delete subsetion
  let deleteSubSessionfn = (e) => {
    dispatch(deleteSubSession(e));
  };

  let sendToSubSesion = (e) => {
    e.preventDefault();

    let id = idsubsession;
    if (!datasub.paid) {
      notifyError("Fiald Paid Is Required");
    }
    if (!datasub.payment_type) {
      notifyError("Fiald payment type Is Required");
    }
    if (!datasub.description) {
      notifyError("Fiald description Is Required");
    }
    if (datasub.description && datasub.payment_type && datasub.paid) {
      dispatch(createSubSession({ data: datasub, id: id }));
    }
    // if (datasub.paid && datasub.payment_type) {
    //   setPopup2(false);
    // }
  };

  let editsubsetion = (e) => {
    e.preventDefault();
    //create subsession
    let editdatasub = {
      payment_type: PaymentEdit,
      paid: PaidEdit,
      description: DescEdit,
      _method: "PUT",
    };
    let data = { data: editdatasub, id: IdEdit };
    if (IdEdit && PaymentEdit && PaidEdit) {
      dispatch(updateSubSession(data));
    }
  };
  // let [newdata, setNewdata] = useState("");
  let ii = 0;
  let newdata =
    dataAccount.length > 0 &&
    dataAccount.map((e) => {
      let textContent = "";
      const parser = new DOMParser();
      const doc = parser.parseFromString(e.description, "text/html");
      textContent = doc.body.textContent;
      return {
        id: e.id,
        name: textContent,
        terminated: e.subSessions.data.length,
        Full: e.full_cost,
        paid: e.paid,
        remaning: e.remaining_cost,
        history: e.subSessions.data.map((el) => {
          return {
            id: el.id,
            date: el.createdAt,
            Fuuull: e.full_cost,
            paid: el.paid,
            Payment_Method: el.payment_type,
            description: el.description,
          };
        }),
      };
    });

  // useEffect(() => {
  //   setNewdata(
  //     dataAccount.length > 0 &&
  //       dataAccount.map((e) => {
  //         let textContent = "";
  //         const parser = new DOMParser();
  //         const doc = parser.parseFromString(e.description, "text/html");
  //         textContent = doc.body.textContent;
  //         return {
  //           id: e.id,
  //           name: textContent,
  //           terminated: e.subSessions.data.length,
  //           Full: e.full_cost,
  //           paid: e.paid,
  //           remaning: e.remaining_cost,
  //           history: e.subSessions.data.map((el) => {
  //             return {
  //               id: el.id,
  //               date: el.createdAt,
  //               Fuuull: e.full_cost,
  //               paid: el.paid,
  //               Payment_Method: el.payment_type,
  //               description: el.description,
  //             };
  //           }),
  //         };
  //       })
  //   );
  // }, [dataAccount]);

  let [toggle, setToggle] = useState(false);
  let [tooth, setTooth] = useState();

  return (
    <div className="row">
      <div className="col-lg-12 mt-1">
        <Navbar />
      </div>
      <div className="col-xl-2 col-lg-12">
        <Sidebar />
      </div>
      <div className="col-xl-10">
        <div className="subse">
          {louser ? (
            <div className="loading">
              <CircularIndeterminate />
            </div>
          ) : (
            <Fragment>
              <div className="row mm singleuser">
                <div className="col-lg-5">
                  {datauser.length > 0 &&
                    datauser.map((e, index) => {
                      return (
                        <div className="box-user" key={index}>
                          <div className="box-img text-center">
                            {e.user_picture ? (
                              <img
                                src={`${BASE_URL}/storage/${e.user_picture}`}
                                alt="..."
                              />
                            ) : (
                              <div className="boxImageChar">
                                <span>
                                  {" "}
                                  {e.first_name.slice(0, 1).toUpperCase()}
                                </span>
                              </div>
                            )}
                          </div>
                          <div className="title">
                            <h3 className="text-center mb-4">
                              {e.first_name + " " + e.last_name}
                            </h3>
                            <div className="detailsitem d-flex justify-content-between mb-2 gap-3">
                              <span className="itemvalue">
                                <EmailIcon className="icon" /> {e.email}
                              </span>
                              <span className="itemvalue">
                                <PhoneIcon className="icon" /> {e.phone_number}
                              </span>
                            </div>
                            <div className="detailsitem d-flex justify-content-between gap-3">
                              <span className="itemvalue">
                                <LocationOnIcon className="icon" />
                                {e.location} /{" "}
                                {e.location_details ? e.location_details : ""}
                              </span>
                              <span className="itemvalue">
                                <HomeIcon className="icon" /> {e.birthday}
                              </span>
                            </div>
                            <div className="d-flex mt-3">
                              <button className="btn btn-outline-primary view mt-2">
                                Message
                              </button>
                              <button
                                className="btn btn-outline-primary view mt-2"
                                onClick={(e) => setPopup(true)}
                              >
                                Add Session
                              </button>
                            </div>
                          </div>
                          {popup && (
                            <form
                              className="overlay"
                              onSubmit={(e) => sendToSesion(e)}
                            >
                              <div className="box">
                                <div className="inp-t">
                                  <label>
                                    Doctor Name :<small>*</small>
                                  </label>
                                  <ComboBox
                                    data={doctors && doctors}
                                    type="doctors"
                                    dataselected={setSelectDoctor}
                                  />
                                </div>

                                <div className="inp-t">
                                  <label>
                                    Diagnosis :<small>*</small>
                                  </label>
                                  <input
                                    type="text"
                                    required
                                    name="description"
                                    placeholder="Pulling the nerve"
                                    onChange={(e) => getdata(e)}
                                  />
                                </div>

                                <div className="inp-t">
                                  <label>
                                    Tooth Number :<small>*</small>
                                  </label>
                                  <input
                                    type="text"
                                    required
                                    readOnly
                                    name="Tooth_Number"
                                    placeholder="Tooth Number"
                                    defaultValue={tooth && tooth}
                                    onChange={(e) => {
                                      getdata(e);
                                    }}
                                    onClick={(e) => {
                                      setToggle(!toggle);
                                    }}
                                  />
                                </div>
                                {toggle && (
                                  <div className="pop-up-tooth">
                                    <Tooth
                                      setToggle={setToggle}
                                      setTooth={setTooth}
                                    />
                                  </div>
                                )}
                                <div className="inp-t">
                                  <label>
                                    Full Cost :<small>*</small>
                                  </label>
                                  <input
                                    onChange={(e) => getdata(e)}
                                    type="number"
                                    required
                                    name="full_cost"
                                    placeholder="90000"
                                  />
                                </div>
                                <div className="inp-t">
                                  <label>
                                    Been Paid :<small>*</small>
                                  </label>
                                  <input
                                    type="number"
                                    required
                                    name="paid"
                                    placeholder="10000"
                                    onChange={(e) => getdata(e)}
                                  />
                                </div>

                                <div className="inp-t">
                                  <label>Description :</label>
                                  <input
                                    type="text"
                                    required
                                    name="test"
                                    placeholder="enter Description"
                                    onChange={(e) => getdata(e)}
                                  />
                                </div>

                                <div className="inp-t">
                                  <label>
                                    Payment Method :<small>*</small>
                                  </label>
                                  <select
                                    style={{
                                      BorderColor: "gray",
                                      padding: "3px 30px",
                                      color: "gray",
                                      width: "100%",
                                    }}
                                    className="form-select"
                                    name="payment_type"
                                    onChange={(e) => getdata(e)}
                                    required
                                    defaultValue={"Chosse Payment Type"}
                                  >
                                    <option
                                      disabled
                                      value={"Chosse Payment Type"}
                                    >
                                      Chosse Payment Type
                                    </option>
                                    <option value={"cash"}>cash</option>
                                    <option value={"card"}>card</option>
                                  </select>
                                </div>
                                <div className="d-flex justify-content-between w-100">
                                  {loadingBTN ? (
                                    <button
                                      disabled={true}
                                      className="submit btn view"
                                    >
                                      loading...
                                    </button>
                                  ) : (
                                    <button className="submit btn view">
                                      Add
                                    </button>
                                  )}

                                  <button
                                    className="submit btn btn-danger"
                                    onClick={(e) => setPopup(false)}
                                  >
                                    cancel
                                  </button>
                                </div>
                              </div>
                            </form>
                          )}
                          {popup2 && (
                            <form
                              className="overlay"
                              onSubmit={(e) => sendToSubSesion(e)}
                            >
                              <div className="box">
                                <div className="inp-t">
                                  <label>
                                    Been Paid :<small>*</small>
                                  </label>
                                  <input
                                    type="number"
                                    name="Been_Paid"
                                    required
                                    placeholder="10000"
                                    onChange={(e) => setsubPaid(e.target.value)}
                                  />
                                </div>
                                <div className="inp-t">
                                  <label>
                                    Description :<small>*</small>
                                  </label>
                                  <textarea
                                    type="text"
                                    className="form-control"
                                    name="Description"
                                    required
                                    placeholder="Write Description  Please"
                                    onChange={(e) =>
                                      setsubDetails(e.target.value)
                                    }
                                  />
                                </div>
                                <div className="inp-t">
                                  <label>
                                    Payment Method :<small>*</small>
                                  </label>
                                  <select
                                    style={{
                                      BorderColor: "gray",
                                      padding: "3px 30px",
                                      color: "gray",
                                      width: "100%",
                                    }}
                                    className="form-select"
                                    name="payment_type"
                                    onChange={(e) =>
                                      setPaymentsub(e.target.value)
                                    }
                                    required
                                    defaultValue={"Chosse Payment Type"}
                                  >
                                    <option
                                      disabled
                                      value={"Chosse Payment Type"}
                                    >
                                      Chosse Payment Type
                                    </option>
                                    <option value={"cash"}>cash</option>
                                    <option value={"card"}>card</option>
                                  </select>
                                </div>
                                <div className="d-flex justify-content-between w-100">
                                  {loadingBTN ? (
                                    <button
                                      disabled={true}
                                      className="submit btn view"
                                    >
                                      loading...
                                    </button>
                                  ) : (
                                    <button className="submit btn view">
                                      Add
                                    </button>
                                  )}
                                  <button
                                    className="submit btn btn-danger"
                                    onClick={(e) => setPopup2(false)}
                                  >
                                    cancel
                                  </button>
                                </div>
                              </div>
                            </form>
                          )}
                          {popup3 && (
                            <div className="overlay">
                              <form className="box">
                                <div className="inp-t">
                                  <label>
                                    Been Paid :<small>*</small>
                                  </label>
                                  <input
                                    type="number"
                                    name="Been_Paid"
                                    placeholder="10000"
                                    defaultValue={PaidEdit}
                                    onChange={(e) =>
                                      setPaidEdit(e.target.value)
                                    }
                                  />
                                </div>

                                <div className="inp-t">
                                  <label>
                                    Description :<small>*</small>
                                  </label>
                                  <textarea
                                    type="text"
                                    className="form-control"
                                    name="Description"
                                    required
                                    defaultValue={DescEdit}
                                    placeholder="Write Description  Please"
                                    onChange={(e) =>
                                      setDescEdit(e.target.value)
                                    }
                                  />
                                </div>
                                <div className="inp-t">
                                  <label>
                                    Payment Method :<small>*</small>
                                  </label>
                                  <input
                                    type="text"
                                    name="Payment_Method"
                                    placeholder="Cash"
                                    defaultValue={PaymentEdit}
                                    onChange={(e) =>
                                      setPaymentEdit(e.target.value)
                                    }
                                  />
                                </div>
                                <div className="d-flex justify-content-between w-100">
                                  {loadingBTN ? (
                                    <button
                                      disabled={true}
                                      className="submit btn view"
                                    >
                                      loading...
                                    </button>
                                  ) : (
                                    <button
                                      className=" btn view"
                                      onClick={(e) => editsubsetion(e)}
                                    >
                                      Edit
                                    </button>
                                  )}
                                  <button
                                    className=" btn btn-danger"
                                    onClick={(e) => setPopup3(false)}
                                  >
                                    cancel
                                  </button>
                                </div>
                              </form>
                            </div>
                          )}
                        </div>
                      );
                    })}
                </div>
                <div className="col-lg-7">
                  <Recharts
                    aspect={3 / 1}
                    title="User Spending (Last 6 Months )"
                  />
                </div>
              </div>

              {loading ? (
                <div className="loading">
                  <CircularIndeterminate />
                </div>
              ) : (
                <div className="bottom row">
                  <CollapsibleTable
                    deleteSubSessionfn={deleteSubSessionfn}
                    setPopup3={setPopup3}
                    setIdSubsission={setIdSubsission}
                    setPopup2={setPopup2}
                    setPaidEdit={setPaidEdit}
                    setDescEdit={setDescEdit}
                    deletesesionfn={deletesesionfn}
                    newdata={newdata}
                    setPaymentEdit={setPaymentEdit}
                    setIdEdit={setIdEdit}
                  />
                </div>
              )}
            </Fragment>
          )}
        </div>
      </div>
    </div>
  );
}

export default EditSessionSubPatiensFinancial;
