import React, { Fragment } from "react";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";

function PopUpSessionPateintFi({ type, data, handleClose }) {
  return (
    <Fragment>
      <Typography id="transition-modal-title" variant="h6" component="h2">
        <div className="head">
          <div className="title-head">
            <span>Description</span>
            <CloseIcon className="icon" onClick={handleClose} />
          </div>
        </div>
      </Typography>

      <Typography
        id="transition-modal-description"
        className="cate-modal"
        sx={{ mt: 2 }}
      >
        <form class="row">
          <div className="col-md-12">
            <div className="form-group">{data}</div>
          </div>
        </form>
      </Typography>
    </Fragment>
  );
}

export default PopUpSessionPateintFi;
