import "./login.scss";
import { useNavigate } from "react-router-dom";
import MailOutlineIcon from "@mui/icons-material/MailOutline";
import LockIcon from "@mui/icons-material/Lock";
import { useDispatch, useSelector } from "react-redux";
import { useState } from "react";
import { logdata, loginDoctor } from "../../store/authSlice";

function Login() {
  let dispatch = useDispatch();
  let [type, setType] = useState("Doctor");
  let [inp, setinp] = useState({
    email: "",
    password: "",
    typr: "",
  });

  let [focusemail, setFocusEmail] = useState(false);
  let [focusPassword, setfocusPassword] = useState(false);

  let bluefn = (e) => {
    if (e === "email") {
      setFocusEmail(true);
    }
    if (e === "password") {
      setfocusPassword(true);
    }
  };

  let getdata = (e) => {
    setinp((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };
  let navigate = useNavigate();
  let getvalue = (e) => {
    e.preventDefault();
    if (inp.email && inp.password) {
      // navigate("/");
      dispatch(loginDoctor(inp));
    }
  };

  let { loading, error, authdata } = useSelector(
    (state) => state.persistTheme.authSlice
  );
  return (
    <div className="login">
      <form onSubmit={(e) => getvalue(e)}>
        <div className="box-login row">
          <div className="col-lg-12 inps">
            <div className="inp-icon">
              <MailOutlineIcon className="icon" />
              <input
                name="email"
                type="email"
                autoComplete="off"
                placeholder="Enter your Email.."
                required
                onChange={(e) => {
                  getdata(e);
                }}
                onBlur={() => {
                  bluefn("email");
                }}
                focused={focusemail.toString()}
              />
              <span>and @ and without spical characters</span>
            </div>

            <div className="inp-icon">
              <LockIcon className="icon" />
              <input
                name="password"
                type="password"
                autoComplete="off"
                placeholder="Enter your password.."
                required
                onChange={(e) => {
                  getdata(e);
                }}
              />
              {/* 
                pattern="^(?=.*[0-9].*[0-9].*[0-9].*[0-9].*[0-9].*[0-9])(?=.*[a-zA-Z].*[a-zA-Z]).{6,}$"
                
             onBlur={() => {
                  bluefn("password");
                }}
                focused={focusPassword.toString()}
                 <span>
                Password must have at least 6 numbers, 2 characters, and 1
                special character
              </span>
            */}
            </div>

            <div className="det-inp"></div>
            {loading ? (
              <button className="btn " disabled>
                loading...
              </button>
            ) : (
              <button className="btn ">Login</button>
            )}
            <hr />
            <div className="policy-ask text-center">
              <p>
                By signing up, you agree to our Terms & Condition , Privacy
                Policy
              </p>
            </div>
          </div>
        </div>
      </form>
    </div>
  );
}

export default Login;
