import React, { Fragment, useState } from "react";
import "./CollapsibleTable.scss";
import Box from "@mui/material/Box";
import Collapse from "@mui/material/Collapse";
import IconButton from "@mui/material/IconButton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Tooltip from "@mui/material/Tooltip";
import TableFooter from "@mui/material/TableFooter";
import GetAppIcon from "@mui/icons-material/GetApp";
import * as XLSX from "xlsx";
import EditIcon from "@mui/icons-material/Edit";
import ModalDelete from "../ModalDelete/ModalDelete";
import ModalMulti from "../ModalMulti/ModalMulti";

function CollapsibleTable({
  newdata,
  setPaymentEdit,
  setIdEdit,
  deletesesionfn,
  setPaidEdit,
  setDescEdit,
  setPopup2,
  setIdSubsission,
  setPopup3,
  deleteSubSessionfn,
}) {
  console.log("newdata==>", newdata);
  function Row(props) {
    const { row } = props;
    const [open, setOpen] = useState(false);

    return (
      <Fragment>
        <TableRow sx={{ "& > *": { borderBottom: "unset" } }}>
          <TableCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {row.name}
          </TableCell>
          <TableCell align="right">{row.terminated}</TableCell>
          <TableCell align="right">{row.Full}</TableCell>
          <TableCell align="right">{row.paid}</TableCell>
          <TableCell align="right">{row.remaning}</TableCell>
          <TableCell align="right">
            <div className="action-det">
              {/*  <button
                className="btn btn-danger"
                onClick={(e) => deletesesionfn(row.id)}
              >
                Delele
    </button>*/}
              <ModalDelete text="BigSession" params={row.id} />
            </div>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={7}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography variant="h6" gutterBottom component="div">
                  <div className="d-flex w-100 sskk">
                    <h4> Details</h4>
                    <div className="d-flex gap-1">
                      <button
                        className="btn view"
                        onClick={(e) => {
                          setPopup2(true);
                          setIdSubsission(row.id);
                        }}
                      >
                        Add
                      </button>
                    </div>
                  </div>
                </Typography>
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow>
                      <TableCell>Date</TableCell>
                      <TableCell>Full cost</TableCell>
                      <TableCell align="right">Been paid</TableCell>
                      <TableCell align="right">Payment Method</TableCell>
                      <TableCell align="right">Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.history.map((historyRow) => (
                      <TableRow key={historyRow.date}>
                        <TableCell component="th" scope="row">
                          {historyRow.date}
                        </TableCell>
                        <TableCell>{historyRow.Fuuull}</TableCell>
                        <TableCell align="right">{historyRow.paid}</TableCell>
                        <TableCell align="right">
                          {historyRow.Payment_Method}
                        </TableCell>
                        <TableCell align="right">
                          <div className="action-det">
                            <EditIcon
                              className="icon-sub icon-sub-edit"
                              onClick={(e) => {
                                setPopup3(true);
                                setPaidEdit(historyRow.paid);
                                setDescEdit(historyRow.description);
                                setPaymentEdit(historyRow.Payment_Method);
                                setIdEdit(historyRow.id);
                              }}
                            />
                            {/*     <button
                              className="btn btn-danger"
                              onClick={(e) =>
                                deleteSubSessionfn({
                                  id: historyRow.id,
                                  sesion_id: row.id,
                                })
                              }
                            >
                              Delete
                            </button>*/}
                            <ModalDelete
                              text="subSession"
                              params={{
                                id: historyRow.id,
                                sesion_id: row.id,
                              }}
                            />
                            <ModalMulti
                              params={historyRow.description}
                              text="subSession"
                              type="show"
                            />
                          </div>
                        </TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </Fragment>
    );
  }
  //function for export
  function handleExport() {
    const data = newdata.map((row) => [
      row.name,
      row.terminated,
      row.Full,
      row.paid,
      row.remaning,
    ]);
    const historyData =
      newdata.length > 0 &&
      newdata.reduce((acc, row) => {
        row.history.forEach((h) => {
          acc.push([row.name, h.date, h.Fuuull, h.paid, h.Payment_Method]);
        });
        return acc;
      }, []);
    const ws = XLSX.utils.book_new();
    const tableData = [
      ["Name", "Sessions Terminated", "Full Cost", "Been Paid", "Remaining"],
      ...data,
    ];
    const historyHeader = [
      "Name",
      "Date",
      "Full Cost",
      "Been Paid",
      "Payment Method",
    ];
    const historyTableData = [historyHeader, ...historyData];
    const combinedData = [
      ["Data"],
      ...tableData,
      ["History"],
      ...historyTableData,
    ];
    XLSX.utils.book_append_sheet(
      ws,
      XLSX.utils.aoa_to_sheet(combinedData),
      "Combined Data"
    );
    XLSX.writeFile(ws, "table_data.xlsx");
  }

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell>
              <Tooltip title="Export">
                <IconButton onClick={() => handleExport()}>
                  <GetAppIcon />
                </IconButton>
              </Tooltip>
            </TableCell>
            <TableCell>Diagnosis</TableCell>
            <TableCell align="right">Sessions terminated</TableCell>
            <TableCell align="right">Full cost</TableCell>
            <TableCell align="right">Been paid</TableCell>
            <TableCell align="right">Remaning</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {newdata.length > 0 &&
            newdata.map((row) => <Row key={row.name} row={row} />)}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default CollapsibleTable;
