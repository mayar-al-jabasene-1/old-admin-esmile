import React from "react";
import "./ModalDelete.scss";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import DeleteIcon from "@mui/icons-material/Delete";
import { useTranslation } from "react-i18next";
import DeletePopUpUser from "../../Pages/Folder-users/DeletePopUpUser/DeletePopUpUser";
import DeletePopUpAppointment from "../../Pages/Folder-appointments/DeletePopUpAppointment/DeletePopUpAppointment";
import DeletePopUpDoctors from "../../Pages/Folder-Doctors/DeletePopUpDoctors/DeletePopUpDoctors";
import DeletePopUpSessionPatientFi from "../../Pages/Folder-Accounting/Folder-Patients-financial/DeletePopUpSessionPatientFi/DeletePopUpSessionPatientFi";

function ModalDelete({ params: data, text, type, selectedRowIds }) {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const [t, i18n] = useTranslation();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  return (
    <div>
      <div onClick={handleOpen} className="d-flex gap-2 align-items-center">
        {
          <DeleteIcon
            className=""
            style={{
              cursor: "pointer",
              color: type === "all" ? "#fff" : "crimson",
            }}
          />
        }
        {type === "all" ? t("Delete Selected") : ""}
      </div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style}>
            <Typography id="transition-modal-title" variant="h6" component="h2">
              <div className="head">
                <div className="title-head">
                  {type === "all" ? "Delete Selected" : "Delete"}
                  <CloseIcon className="icon" onClick={handleClose} />
                </div>
              </div>
            </Typography>

            {text === "user" ? (
              <DeletePopUpUser
                data={data}
                handleClose={handleClose}
                type={type}
                selectedRowIds={selectedRowIds}
              />
            ) : text === "appointment" ? (
              <DeletePopUpAppointment
                data={data}
                handleClose={handleClose}
                type={type}
                selectedRowIds={selectedRowIds}
              />
            ) : text === "doctor" ? (
              <DeletePopUpDoctors
                data={data}
                handleClose={handleClose}
                type={type}
                selectedRowIds={selectedRowIds}
              />
            ) : text === "subSession" || text === "BigSession" ? (
              <DeletePopUpSessionPatientFi
                data={data}
                handleClose={handleClose}
                type={type}
                text={"BigSession"}
                selectedRowIds={selectedRowIds}
              />
            ) : (
              ""
            )}
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}

export default ModalDelete;
