import * as React from "react";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";

export default function ComboBox({ data, type, dataselected }) {
  let top100Films2;
  if (type === "patients" || type === "doctors") {
    top100Films2 =
      data &&
      data.map((e) => {
        return { name: e.first_name + " " + e.last_name, id: e.id };
      });
  }

  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      options={
        top100Films2 &&
        top100Films2.map((item) => ({
          label: item.name,
          value: item.id,
        }))
      }
      sx={{ width: 300 }}
      renderInput={(params) => <TextField {...params} label={type} />}
      onChange={(event, newValue) => {
        dataselected(newValue); // Pass the selected value to the dataselected function
      }}
    />
  );
}
