import "./Navbar.scss";
import NotificationsIcon from "@mui/icons-material/Notifications";
import SegmentIcon from "@mui/icons-material/Segment";
import WbSunnyIcon from "@mui/icons-material/WbSunny";
import ChatBubbleIcon from "@mui/icons-material/ChatBubble";
import imgprof from "../../assists/IMG_profile.JPG";
import img1 from "../../assists/logo-1.png";
import { useState } from "react";
import { showsliderfn } from "../../store/userslice";
import { useDispatch, useSelector } from "react-redux";
import { toggle } from "../../store/theme";
import { logout } from "../../store/authSlice";
import * as React from "react";
import Button from "@mui/material/Button";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import Fade from "@mui/material/Fade";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import { useNavigate } from "react-router-dom";
import { BASE_URL } from "../../apiConfig";
import DarkModeIcon from "@mui/icons-material/DarkMode";
function FadeMenu({ lang, profile }) {
  let dispatch = useDispatch();
  let navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className="dropdown12">
      <Button
        id="fade-button"
        aria-controls={open ? "fade-menu" : undefined}
        aria-haspopup="true"
        aria-expanded={open ? "true" : undefined}
        onClick={handleClick}
      >
        {profile && <KeyboardArrowDownIcon />}
      </Button>
      <Menu
        id="fade-menu"
        MenuListProps={{
          "aria-labelledby": "fade-button",
        }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
      >
        {profile && (
          <div>
            <MenuItem onClick={handleClose}>
              <div className="text-center name-profile">ESmile</div>
            </MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem
              onClick={() => {
                handleClose();
                dispatch(logout());
                navigate("/");
              }}
            >
              Logout
            </MenuItem>
          </div>
        )}

        {lang && (
          <div>
            <MenuItem onClick={handleClose}>English</MenuItem>
            <MenuItem onClick={handleClose}>Arabic</MenuItem>
          </div>
        )}
      </Menu>
    </div>
  );
}

function Navbar() {
  let dispatch = useDispatch();
  let [notiIn, setNotiIn] = useState(false);
  let { dark } = useSelector((state) => state.persistTheme.themeslice);

  let clickfn = () => {
    dispatch(toggle());
  };

  let { authdata } = useSelector((state) => state.persistTheme.authSlice);
  let newdata = authdata && authdata.data["0"]["0"];
  return (
    <div className="topbar">
      <div className="topbar_wrapper">
        <div className="logo pb-2">
          <img src={img1} alt="..." />
        </div>
        <div className="big-icons-nav">
          {/*<div className="icon-topbar">
            <span className="sp" onClick={() => setNotiIn(!notiIn)}>
              1
            </span>
            <NotificationsIcon onClick={() => setNotiIn(!notiIn)} />

            {notiIn && (
              <div className="box-noti-pop">
                <div className="title-nafi" onClick={() => setNotiIn(false)}>
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
                <div className="title-nafi">
                  <img
                    src="https://images.pexels.com/photos/1933873/pexels-photo-1933873.jpeg?auto=compress&cs=tinysrgb&w=600"
                    alt="..."
                  />
                  <div>
                    <span className="name">Mayar AL jabasene</span> book an
                    appointment now at :
                    <span className="date">2023/15/7 10:30 Am</span>
                  </div>
                </div>
              </div>
            )}
          </div>
            <div className="icon-topbar">
            <span className="sp">2</span>
            <ChatBubbleIcon />
          </div> */}
          <div
            className="icon-topbar"
            onClick={(e) => {
              clickfn(e);
            }}
          >
            {dark ? (
              <WbSunnyIcon style={{ cursor: "pointer" }} />
            ) : (
              <DarkModeIcon style={{ cursor: "pointer" }} />
            )}
          </div>
          <div className="icon-topbar mmm">
            <div className="Title">
              {newdata.doctor_picture ? (
                <img
                  className="cellImg"
                  src={`${BASE_URL}/storage/${newdata.doctor_picture}`}
                  alt="avatar"
                />
              ) : (
                <div className="boxImageChar">
                  <span> {newdata.first_name.slice(0, 1).toUpperCase()}</span>
                </div>
              )}
            </div>
          </div>
          <div className="icon-topbar list">
            <SegmentIcon
              style={{ cursor: "pointer" }}
              onClick={() => dispatch(showsliderfn())}
            />
          </div>
          <FadeMenu profile="profile" />
        </div>
      </div>
    </div>
  );
}

export default Navbar;
