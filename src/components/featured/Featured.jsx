import "./Featured.scss";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
function Featured() {
  return (
    <div className="featured">
      <div className="top-fea">
        <h3>Total Revenue</h3>
        <MoreVertIcon />
      </div>
      <div className="mid-fea">
        <span className="circle">
          <CircularProgressbar value={70} text="70%" stroke="#2CD8AF" />
        </span>
        <h6>Total Appointments Today</h6>
        <p className="number">30</p>
        <p className="title">
          previous transaction processing last payments may not be included
        </p>
      </div>
      <div className="bottom-fea">
        <div className="mark-e">
          <span>Receive</span>
          <p>100$</p>
        </div>
        <div className="mark-e">
          <span>Remaining</span>
          <p>20$</p>
        </div>
        <div className="mark-e">
          <span>Profit</span>
          <p>20%</p>
        </div>
      </div>
    </div>
  );
}

export default Featured;
