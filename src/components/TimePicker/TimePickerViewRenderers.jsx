import * as React from "react";
import "./TimePickerViewRenderers.scss";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import { renderTimeViewClock } from "@mui/x-date-pickers/timeViewRenderers";
import dayjs from "dayjs";

export default function TimePickerViewRenderers({
  getdataAppo,
  type,
  index,
  value,
  name,
  SingleDoctor,
  dataSingleAppo,
  SelDate,
}) {
  const selectedDate = dayjs(SelDate);
  const disabledTimes =
    dataSingleAppo &&
    dataSingleAppo.map((appointment) =>
      dayjs(appointment.selected_time, "HH:mm")
    );

  const availabilityForSelectedDay =
    SingleDoctor &&
    SingleDoctor?.availability.find(
      (availability) =>
        availability.day_of_week === selectedDate.format("dddd").toLowerCase()
    );

  const minTime = dayjs(availabilityForSelectedDay?.start_time, "HH:mm:ss");
  const maxTime = dayjs(
    availabilityForSelectedDay?.end_time,
    "HH:mm:ss"
  ).subtract(30, "minutes");

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      {SelDate ? (
        <DemoContainer components={["TimePicker"]}>
          <TimePicker
            label={
              name === "edit"
                ? `previous value : ${value}`
                : "Choose Time, please"
            }
            onChange={(newDate) => {
              const formattedTime = newDate.format("HH:mm");
              getdataAppo(formattedTime, type, index);
            }}
            readOnly={SelDate ? false : true}
            viewRenderers={{
              hours: renderTimeViewClock,
              minutes: renderTimeViewClock,
              seconds: renderTimeViewClock,
            }}
            ampm={false}
            defaultValue={SelDate && dayjs("2022-04-17T00:30")}
            minutesStep={30}
            minTime={minTime}
            maxTime={maxTime}
            shouldDisableTime={(time) => {
              // Convert the time to the same format as in dataSingleAppo
              const formattedTime = time.format("HH:mm");

              // Check if the formatted time is in the list of disabled times
              return (
                dataSingleAppo &&
                dataSingleAppo.some(
                  (appointment) => appointment.selected_time === formattedTime
                )
              );
            }}
          />
        </DemoContainer>
      ) : (
        <DemoContainer components={["TimePicker"]}>
          <TimePicker
            label={
              name === "edit"
                ? `previous value : ${value}`
                : "Choose Time, please"
            }
            onChange={(newDate) => {
              const formattedTime = newDate.format("HH:mm");
              getdataAppo(formattedTime, type, index);
            }}
            readOnly={SelDate ? false : true}
            viewRenderers={{
              hours: renderTimeViewClock,
              minutes: renderTimeViewClock,
              seconds: renderTimeViewClock,
            }}
            ampm={false}
            minutesStep={30}
            minTime={minTime}
            maxTime={maxTime}
            shouldDisableTime={(time) => {
              // Convert the time to the same format as in dataSingleAppo
              const formattedTime = time.format("HH:mm");

              // Check if the formatted time is in the list of disabled times
              return (
                dataSingleAppo &&
                dataSingleAppo.some(
                  (appointment) => appointment.selected_time === formattedTime
                )
              );
            }}
          />
        </DemoContainer>
      )}
    </LocalizationProvider>
  );
}
