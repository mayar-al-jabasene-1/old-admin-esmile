import "./Widgate.scss";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import PersonIcon from "@mui/icons-material/Person";
import EventNoteIcon from "@mui/icons-material/EventNote";
import MonetizationOnIcon from "@mui/icons-material/MonetizationOn";
import AccountBalanceWalletIcon from "@mui/icons-material/AccountBalanceWallet";

function Widget({ type }) {
  let data;
  switch (type) {
    case "appointments":
      data = {
        h3: "Appointment",
        number: "20",
        p: 80,
        ismoney: false,
        state: "pos",
        title: "See all appointments",
        icon: (
          <EventNoteIcon
            className="icon"
            style={{
              color: "crimson",
              backgroundColor: "rgba(255,0,0,0.2)",
              borderRadius: "5px",
              padding: "2px",
            }}
          />
        ),
      };
      break;

    case "users":
      data = {
        h3: "users",
        number: "20",
        p: 80,
        ismoney: false,
        state: "neg",
        title: "See all users",
        icon: (
          <PersonIcon
            className="icon"
            style={{
              color: "goldenrod",
              backgroundColor: "rgba(218,165,32,0.2)",
              borderRadius: "5px",
              padding: "2px",
            }}
          />
        ),
      };
      break;

    case "earning":
      data = {
        h3: "earning",
        number: "20",
        p: 2000,
        ismoney: true,
        state: "pos",
        title: "Veiw net earning",
        icon: (
          <MonetizationOnIcon
            className="icon"
            style={{
              color: "green",
              backgroundColor: "rgba(0,128,0,0.2)",
              borderRadius: "5px",
              padding: "2px",
            }}
          />
        ),
      };
      break;

    case "balance":
      data = {
        h3: "balance",
        number: "20",
        p: 400,
        ismoney: true,
        state: "pos",
        title: "See Detalis",
        icon: (
          <AccountBalanceWalletIcon
            className="icon"
            style={{
              color: "purple",
              backgroundColor: "rgba(128,0,128,0.2)",
              borderRadius: "5px",
              padding: "2px",
            }}
          />
        ),
      };
      break;

    default:
      break;
  }

  return (
    <div className="widgate">
      <div className="top-widgate">
        <h3>{data.h3}</h3>
        <span className={data.state}>
          {data.state === "pos" ? <KeyboardArrowUpIcon /> : <ExpandMoreIcon />}
          {data.number}%
        </span>
      </div>
      <p>
        {data.ismoney && "$"} {data.p}
      </p>
      <div className="bottom-widgate">
        <span>{data.title}</span>
        {data.icon}
      </div>
    </div>
  );
}

export default Widget;
