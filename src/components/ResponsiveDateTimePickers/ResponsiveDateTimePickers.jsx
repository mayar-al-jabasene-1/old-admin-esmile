import React from "react";
import "./ResponsiveDateTimePickers.scss";
import { DemoContainer } from "@mui/x-date-pickers/internals/demo";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import dayjs from "dayjs";
import { useDispatch } from "react-redux";
import { getDoctorAppointments } from "../../store/appointmentSlice";

function ResponsiveDateTimePickers({
  selectedDate,
  setSelectedDate,
  SingleDoctor,
}) {
  // Extract available days from the availability array
  const availableDays = SingleDoctor?.availability?.map(
    (availability) => availability.day_of_week
  );

  // Define a function to check if a date is disabled
  const shouldDisableDate = (date) => {
    const dayOfWeek = date.format("dddd").toLowerCase();
    const currentDate = dayjs();

    return (
      (availableDays && !availableDays.includes(dayOfWeek)) ||
      date.isBefore(currentDate, "day")
    );
  };

  const minTime = dayjs().set("hour", 8).set("minute", 0);
  const maxTime = dayjs().set("hour", 16).set("minute", 0);

  let dispatch = useDispatch();

  let sendDate = (e) => {
    dispatch(
      getDoctorAppointments({
        id: SingleDoctor.id,
        date: e.format("YYYY-MM-DD"),
      })
    );
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <DemoContainer components={["DatePicker"]}>
        <DatePicker
          label="Basic date picker"
          readOnly={SingleDoctor ? false : true}
          shouldDisableDate={shouldDisableDate}
          onChange={(newDate) => {
            setSelectedDate(newDate);
            sendDate(newDate);
          }}
        />
      </DemoContainer>
    </LocalizationProvider>
  );
}

export default ResponsiveDateTimePickers;
