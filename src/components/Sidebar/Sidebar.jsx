import img1 from "../../assists/logo-1.png";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import EngineeringIcon from "@mui/icons-material/Engineering";
import EventNoteIcon from "@mui/icons-material/EventNote";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import StoreMallDirectoryIcon from "@mui/icons-material/StoreMallDirectory";
import DynamicFeedOutlinedIcon from "@mui/icons-material/DynamicFeedOutlined";
import ChatOutlinedIcon from "@mui/icons-material/ChatOutlined";
import NotificationsIcon from "@mui/icons-material/Notifications";
import QuestionAnswerIcon from "@mui/icons-material/QuestionAnswer";
import SettingsSuggestIcon from "@mui/icons-material/SettingsSuggest";
import PointOfSaleIcon from "@mui/icons-material/PointOfSale";
import LogoutIcon from "@mui/icons-material/Logout";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";
import CreateNewFolderIcon from "@mui/icons-material/CreateNewFolder";
import AutoStoriesIcon from "@mui/icons-material/AutoStories";
import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import ArrowDropUpIcon from "@mui/icons-material/ArrowDropUp";
import ArrowRightIcon from "@mui/icons-material/ArrowRight";
import AccountBalanceWalletIcon from "@mui/icons-material/AccountBalanceWallet";
import ManageAccountsIcon from "@mui/icons-material/ManageAccounts";

import "./sidebar.scss";
import { NavLink } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clickble } from "../../store/theme";
import { useTranslation } from "react-i18next";
import { useState } from "react";

function Sidebar() {
  let dispatch = useDispatch();
  const [t, i18n] = useTranslation();
  let clickfn = (e) => {
    dispatch(clickble(e));
  };
  let [showMenu, setShowMenu] = useState({});
  let [toggle, setToggle] = useState(true);
  let toggleMenu = (n) => {
    setToggle(!toggle);
    setShowMenu({ name: n, state: toggle });
  };

  let { showslider } = useSelector((state) => state.users);
  return (
    <div className={!showslider ? "big" : "big2"}>
      <div className="list">
        <h6>Main</h6>
        <ul>
          <NavLink to="/">
            <DashboardIcon className="icon-s" />
            Home
          </NavLink>
        </ul>
      </div>

      <div className="list">
        <h6>List</h6>
        <ul className="list-unstyled">
          <NavLink to="/users">
            <PeopleAltIcon className="icon-s" />
            Patients Portal
          </NavLink>
          <NavLink to="/doctors">
            <EngineeringIcon className="icon-s" />
            Workforce
          </NavLink>

          <NavLink to="/appointments">
            <EventNoteIcon className="icon-s" />
            Appointment
          </NavLink>

          <NavLink to="/Patients-financial">
            <AccountBalanceWalletIcon className="icon-s" />
            {t("Sessions And Finance")}
          </NavLink>

          <NavLink to="/roles-permision">
            <ManageAccountsIcon className="icon-s" />
            {t("Roles And Permission")}
          </NavLink>

          {/*<NavLink to="/accounting">
            <CreateNewFolderIcon className="icon-s" />
            Accounting
  </NavLink>*/}
        </ul>
      </div>

      <div className="list">
        <h6>{t("Financial Management")}</h6>
        <ul className="list-unstyled">
          <a onClick={() => toggleMenu("Accounting")}>
            <CreateNewFolderIcon className="icon-s" />
            {t("Accounting")}
            {showMenu.name === "Accounting" && showMenu.state ? (
              <ArrowDropDownIcon style={{ margin: "auto" }} />
            ) : (
              <ArrowDropUpIcon style={{ margin: "auto" }} />
            )}
          </a>

          <ul
            className={`list-unstyled menu-list ${
              showMenu.name === "Accounting" && showMenu.state ? "active" : ""
            }`}
          >
            {/*   <NavLink to="/Patients-financial">
          <ArrowRightIcon className="icon-s" />
          {t("Patients Financial Accounts")}
        </NavLink> */}

            <NavLink to="/store">
              <StoreMallDirectoryIcon className="icon-s" />
              Store
            </NavLink>

            <NavLink to="/order">
              <PointOfSaleIcon className="icon-s" />
              {t("Orders")}
            </NavLink>
          </ul>
        </ul>
      </div>

      {/**      <div className="list">
        <h6>Shop</h6>
        <ul className="list-unstyled">
          
          <NavLink to="/order">
            <DynamicFeedOutlinedIcon className="icon-s" />
            Orders
          </NavLink>
        </ul>
      </div>

     <div className="list">
        <h6>USEFUL</h6>
        <ul className="list-unstyled">
              <NavLink to="/notifications">
          <NotificationsIcon className="icon-s" />
          Notifications
        </NavLink> 
        <NavLink to="/chat">
        <ChatOutlinedIcon className="icon-s" />
        Chat
      </NavLink>
      <NavLink to="/questionAnswer">
        <QuestionAnswerIcon className="icon-s" />
        QuestionAnswer
      </NavLink>
    </ul>
  </div> */}

      <div className="list">
        <h6>Settings</h6>
        <ul className="list-unstyled">
          <NavLink to="/SystemHealth">
            <SettingsSuggestIcon className="icon-s" />
            Cookie Policy
          </NavLink>
          <NavLink to="/settings">
            <SettingsSuggestIcon className="icon-s" />
            Privacy Policy
          </NavLink>
        </ul>
      </div>

      <div className="list">
        <h6>Profile</h6>
        <ul className="list-unstyled">
          <NavLink to="/profile">
            <AccountCircleIcon className="icon-s" />
            My Profile
          </NavLink>
          <div>
            <LogoutIcon className="icon-s" />
            Logout
          </div>
        </ul>
      </div>

      <div className="list">
        <h6>Theme</h6>
        {/**<div className="themesfather">
        <div className="theme white"></div>
        <div className="theme dark"></div>
      </div>**/}
        <div className="themesfather">
          <div
            className="theme white"
            onClick={() => {
              clickfn(false);
            }}
          ></div>
          <div
            className="theme dark"
            onClick={() => {
              clickfn(true);
            }}
          ></div>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;
