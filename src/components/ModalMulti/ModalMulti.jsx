import React, { Fragment, useState } from "react";
import "./ModalMulti.scss";
import Backdrop from "@mui/material/Backdrop";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Fade from "@mui/material/Fade";
import VisibilityIcon from "@mui/icons-material/Visibility";
import EditIcon from "@mui/icons-material/Edit";
import NoteAltIcon from "@mui/icons-material/NoteAlt";
import AddIcon from "@mui/icons-material/Add";
import { useTranslation } from "react-i18next";
import PopUpAppointment from "../../Pages/Folder-appointments/PopUpAppointment/PopUpAppointment";
import ZoomInIcon from "@mui/icons-material/ZoomIn";
import PopUpSessionPateintFi from "../../Pages/Folder-Accounting/Folder-Patients-financial/PopUpSessionPateintFi/PopUpSessionPateintFi";
import PopUpDoctors from "../../Pages/Folder-Doctors/PopUpDoctors/PopUpDoctors";
import PopUpUser from "../../Pages/Folder-users/PopUpUser/PopUpUser";

function ModalMulti({ params: data, text, type, type2 }) {
  const [t, i18n] = useTranslation();
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const label = { inputProps: { "aria-label": "Size switch demo" } };

  return (
    <div>
      <div onClick={handleOpen} className="d-flex gap-2 align-items-center">
        {type === "viwe" ? (
          <VisibilityIcon style={{ cursor: "pointer" }} />
        ) : type === "edit" ? (
          <EditIcon style={{ cursor: "pointer" }} />
        ) : type === "show" ? (
          <NoteAltIcon style={{ cursor: "pointer" }} />
        ) : (
          <AddIcon />
        )}
      </div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}
      >
        <Fade in={open}>
          <Box sx={style} className="mmmm">
            {text === "appointment" ? (
              <PopUpAppointment
                type="show"
                data={data}
                handleClose={handleClose}
              />
            ) : text === "subSession" ? (
              <PopUpSessionPateintFi
                type="show"
                data={data}
                handleClose={handleClose}
              />
            ) : text === "Doctors" ? (
              <PopUpDoctors type="viwe" data={data} handleClose={handleClose} />
            ) : text === "Users" ? (
              <PopUpUser type="viwe" data={data} handleClose={handleClose} />
            ) : (
              ""
            )}
          </Box>
        </Fade>
      </Modal>
    </div>
  );
}

export default ModalMulti;
