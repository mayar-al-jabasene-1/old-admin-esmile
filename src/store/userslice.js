import axios from "axios";
import { BASE_URL } from "../apiConfig"; // Adjust the path based on your project structure
import { notifyError, notifysuccess } from "../Notification";
const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

export let createuser = createAsyncThunk(
  "users/createuser",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let res = await axios.post(`${BASE_URL}/api/users/register`, arg, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
      return res.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getusers = createAsyncThunk(
  "users/getusers",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let data = await axios.get(`${BASE_URL}/api/users/getAllUsers`);
      return data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let deleteuser = createAsyncThunk(
  "users/deleteuser",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      const idList = Array.isArray(arg.id) ? arg.id : [arg.id]; // Ensure id is an array
      const promises = idList.map((id) =>
        axios.post(`${BASE_URL}/api/users/deleteUser/${id}`, arg.data, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
      );

      // Wait for all deletion requests to complete
      const responses = await Promise.all(promises); // Wait for all deletion requests to complete

      return { idList: idList, responses: responses }; // Return the list of deleted IDs
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getsingeuser = createAsyncThunk(
  "getsingeuser/users",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;

    try {
      let data = await axios.get(
        `${BASE_URL}/api/users/getUserById/${arg}`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let updateuser = createAsyncThunk(
  "updateuser/users",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let data = await axios.post(
        `${BASE_URL}/api/users/updateUser/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

let userslice = createSlice({
  name: "users",
  initialState: {
    loading: false,
    laodingBTN: false,
    data: [],
    dataCreate: false,
    error: false,
    showslider: false,
    loadingSingle: false,
    datalog: false,
    errorSingle: false,
    dataupdate: false,
    datasingle: false,
  },
  reducers: {
    showsliderfn: (state, action) => {
      state.showslider = !state.showslider;
    },
    resetDataCreateAction: (state, action) => {
      state.dataCreate = action.payload;
    },
    resetDataUpdateAction: (state, action) => {
      state.dataupdate = action.payload;
    },
  },
  extraReducers: {
    //createuser
    [createuser.pending]: (state, action) => {
      state.laodingBTN = true;
    },
    [createuser.fulfilled]: (state, action) => {
      state.dataCreate = action.payload;
      state.laodingBTN = false;
      notifysuccess(action.payload.message);
      state.error = false;
    },
    [createuser.rejected]: (state, action) => {
      state.laodingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getusers

    [getusers.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
      state.dataCreate = false;
    },
    [getusers.fulfilled]: (state, action) => {
      state.loading = false;
      state.data = action.payload;
      state.dataCreate = false;
    },
    [getusers.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.dataCreate = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //deleteuser
    [deleteuser.pending]: (state, action) => {
      state.loadingBTN = true;
    },
    [deleteuser.fulfilled]: (state, action) => {
      state.data = state.data.filter(
        (e) => !action.payload.idList.includes(e.id)
      );
      state.loadingBTN = false;
      state.error = false;
      notifysuccess(action.payload.responses[0].data.message);
    },
    [deleteuser.rejected]: (state, action) => {
      state.loadingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getsingeuser
    [getsingeuser.pending]: (state, action) => {
      state.loadingSingle = true;

      state.error = false;
    },
    [getsingeuser.fulfilled]: (state, action) => {
      state.loadingSingle = false;
      state.datasingle = action.payload;
    },
    [getsingeuser.rejected]: (state, action) => {
      state.loadingSingle = false;
      state.errorSingle = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //updateuser
    [updateuser.pending]: (state, action) => {
      state.laodingBTN = true;
      state.error = false;
    },
    [updateuser.fulfilled]: (state, action) => {
      state.laodingBTN = false;
      state.dataupdate = action.payload;
      notifysuccess(action.payload.message);
    },
    [updateuser.rejected]: (state, action) => {
      state.laodingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
  },
});

export default userslice.reducer;
export let { showsliderfn, resetDataCreateAction, resetDataUpdateAction } =
  userslice.actions;
