import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { BASE_URL } from "../apiConfig";
import { notifyError, notifysuccess } from "../Notification";
export let createApoointment = createAsyncThunk(
  "createApoointment/appo",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let data = await axios.post(
        `${BASE_URL}/api/appointments/createFromDoctor/${arg.doctor_id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getAllappointment = createAsyncThunk(
  "getAllappointment/appo",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let data = await axios.get(
        `${BASE_URL}/api/appointments/getAllAppointments`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let deleteappointment = createAsyncThunk(
  "deleteappointment/appo",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      const idList = Array.isArray(arg.id) ? arg.id : [arg.id]; // Ensure id is an array
      const promises = idList.map((id) =>
        axios.post(
          `${BASE_URL}/api/appointments/deleteAppointment/${id}`,
          arg.data,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
      );

      // Wait for all deletion requests to complete
      const responses = await Promise.all(promises); // Wait for all deletion requests to complete

      return { idList: idList, responses: responses }; // Return the list of deleted IDs
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getuserappointment = createAsyncThunk(
  "getuserappointment/appo",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let data = await axios.get(
        `${BASE_URL}/api/appointments/getUserAppointments/${arg}`,
        arg,
        { headers: { "Content-Type": "application/json;charset=utf-8" } }
      );
      return data.data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getDoctorAppointments = createAsyncThunk(
  "auth/getDoctorAppointments",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let res = await axios.get(
        `${BASE_URL}/api/appointments/getDoctorAppointments/${arg.id}?selected_date=${arg.date}`,
        arg,
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      return res.data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

let appointmentslice = createSlice({
  name: "appo",
  initialState: {
    loading: false,
    data: [],
    dataCreate: false,
    error: false,
    note: false,
    dataNote: "",
    datasingle: false,
    loadingBTN: false,
    loadingSingle: false,
    dataSingleAppo: false,
    laodingApo: false,
  },
  reducers: {
    getdatanote: (state, action) => {
      state.note = !state.note;
      state.dataNote = action.payload;
    },
    resetDataCreateAction: (state, action) => {
      state.dataCreate = action.payload;
    },
    resetDataUpdateAction: (state, action) => {
      state.dataUpdate = action.payload;
    },
  },
  extraReducers: {
    [createApoointment.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [createApoointment.fulfilled]: (state, action) => {
      state.loading = false;
      state.dataCreate = action.payload;
      notifysuccess(action.payload.message);
    },
    [createApoointment.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getDoctorAppointments  doctor

    [getDoctorAppointments.pending]: (state, action) => {
      state.laodingApo = true;
      state.error = false;
    },
    [getDoctorAppointments.fulfilled]: (state, action) => {
      state.dataSingleAppo = action.payload;
      state.laodingApo = false;
      state.error = false;
    },
    [getDoctorAppointments.rejected]: (state, action) => {
      state.laodingApo = false;
      // notifyError(
      //   action.payload?.message && action.payload.message
      //     ? action.payload.message
      //     : action.payload
      // );
    },

    //getallappointment
    [getAllappointment.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
      state.data = false;
    },
    [getAllappointment.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
    },
    [getAllappointment.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
    //delete deleteappointment
    [deleteappointment.pending]: (state, action) => {
      state.loadingBTN = true;
      state.error = false;
    },
    [deleteappointment.fulfilled]: (state, action) => {
      state.data = state.data.filter(
        (e) => !action.payload.idList.includes(e.id)
      );
      state.loadingBTN = false;
      state.error = false;
      notifysuccess(action.payload.responses[0].data.message);
    },
    [deleteappointment.rejected]: (state, action) => {
      state.loadingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
    //getuserappointment user
    [getuserappointment.pending]: (state, action) => {
      state.data = [];
      state.loadingSingle = true;
      state.error = false;
    },
    [getuserappointment.fulfilled]: (state, action) => {
      state.datasingle = action.payload;
      state.loadingSingle = false;
    },
    [getuserappointment.rejected]: (state, action) => {
      state.loadingSingle = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
  },
});

export default appointmentslice.reducer;
export let { getdatanote, resetDataCreateAction, resetDataUpdateAction } =
  appointmentslice.actions;
