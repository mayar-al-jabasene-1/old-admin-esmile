import { configureStore, combineReducers } from "@reduxjs/toolkit";
import users from "./userslice";
import doctorSlice from "./DoctorSlice";
import appointments from "./appointmentSlice";
import patientsfinancialSlice from "./patientsfinancialSlice";
import themeslice from "./theme";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";
import authSlice from "./authSlice";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
};

const customReducer = combineReducers({
  themeslice: themeslice,
  authSlice: authSlice,
});

const persistCustomReducer = persistReducer(persistConfig, customReducer);

const rootReducer = combineReducers({
  persistTheme: persistCustomReducer,
  users: users,
  doctors: doctorSlice,
  appointments: appointments,
  patientsfinancial: patientsfinancialSlice,
});

const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    }),
});

export const persistor = persistStore(store);

export default store;
