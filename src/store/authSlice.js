import axios from "axios";

import { BASE_URL } from "../apiConfig";
import { notifyError, notifysuccess } from "../Notification";
const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

// //************************admin************************************//
// export let regidata = createAsyncThunk(
//   "auth/rigdata",
//   async (arg, thunkAPI) => {
//     let { rejectWithValue } = thunkAPI;
//     try {
//       let res = await axios.post(`${BASE_URL}/api/admins/createAdmin`, arg, {
//         headers: {
//           "Content-Type": "multipart/form-data",
//         },
//       });
//       return res.data.data.data;
//     } catch (error) {
//       return rejectWithValue(error.response.data.errors);
//     }
//   }
// );

// //************************admin************************************//
// export let logdata = createAsyncThunk("auth/logdata", async (arg, thunkAPI) => {
//   let { rejectWithValue } = thunkAPI;
//   try {
//     let res = await axios.post(`${BASE_URL}/api/admins/loginAdmin`, arg, {
//       headers: { "Content-Type": "application/json" },
//     });
//     return res.data.data["0"];
//   } catch (error) {
//     return rejectWithValue(error.response.data.message);
//   }
// });

// //************************admin************************************//
// export let updatedata = createAsyncThunk(
//   "auth/updatedata",
//   async (arg, thunkAPI) => {
//     let { rejectWithValue, getState } = thunkAPI;
//     let { id } = getState().authslice.data.data;
//     try {
//       let res = await axios.put(
//         `${BASE_URL}/api/admins/updateAdmin/${id}`,
//         JSON.stringify(arg),
//         {
//           headers: { "Content-Type": "application/json" },
//         }
//       );

//       return res.data.data.data;
//     } catch (error) {
//       return rejectWithValue(error);
//     }
//   }
// );

// //************************admin************************************//
// export let getAllAdmins = createAsyncThunk(
//   "auth/getAllAdmins",
//   async (arg, thunkAPI) => {
//     let { rejectWithValue } = thunkAPI;
//     try {
//       let res = await axios.get(`${BASE_URL}/api/admins/getAllAdmins`, arg, {
//         headers: { "Content-Type": "application/json" },
//       });
//       return res.data.data.data;
//     } catch (error) {
//       return rejectWithValue(error.message);
//     }
//   }
// );

// //************************admin************************************//
// export let getAdminById = createAsyncThunk(
//   "auth/getAdminById",
//   async (arg, thunkAPI) => {
//     let { rejectWithValue } = thunkAPI;
//     try {
//       let res = await axios.get(
//         `${BASE_URL}/api/admins/getAdminById/${arg}`,
//         arg,
//         {
//           headers: { "Content-Type": "application/json" },
//         }
//       );
//       return res.data.data.data;
//     } catch (error) {
//       return rejectWithValue(error.message);
//     }
//   }
// );

// //************************admin************************************//
// export let deleteadmin = createAsyncThunk(
//   "users/deleteadmin",
//   async (arg, ThunkAPI) => {
//     let { rejectWithValue } = ThunkAPI;
//     try {
//       let data = await axios.delete(
//         `${BASE_URL}/api/admins/deleteAdmin/${arg}`,
//         arg,
//         {
//           headers: {
//             "Content-Type": "application/json;charset=utf-8",
//           },
//         }
//       );
//       return arg;
//     } catch (error) {
//       return rejectWithValue(error.message);
//     }
//   }
// );

// //************************doctors************************************//
// export let register = createAsyncThunk(
//   "auth/register",
//   async (arg, thunkAPI) => {
//     let { rejectWithValue } = thunkAPI;
//     try {
//       let res = await axios.post(
//         `${BASE_URL}/api/doctors/registerDoctor`,
//         arg,
//         {
//           headers: {
//             "Content-Type": "multipart/form-data",
//           },
//         }
//       );
//       return res.data;
//     } catch (error) {
//       return rejectWithValue(
//         error.response ? error.response.data : error.message
//       );
//     }
//   }
// );

//************************doctors************************************//
export let loginDoctor = createAsyncThunk(
  "auth/login",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let res = await axios.post(`${BASE_URL}/api/doctors/loginDoctor`, arg, {
        headers: { "Content-Type": "application/json" },
      });
      return res.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

let authslice = createSlice({
  name: "auth",
  initialState: {
    data: false,
    dataAll: false,
    authdata: false,
    loading: false,
    error: false,
    message: false,
    datalog: false,
    loadingDoc: false,
    createDoc: false,
    dataSingle: false,
    laodingBTN: false,
  },
  reducers: {
    logout: (state, action) => {
      state.authdata = null;
    },
    datalogfn: (state, action) => {
      state.datalog = action.payload;
    },
  },
  extraReducers: {
    // //regidata  admin

    // [regidata.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    // },
    // [regidata.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.error = false;
    //   state.authdata = action.payload;
    // },
    // [regidata.rejected]: (state, action) => {
    //   state.loading = false;
    //   state.error = action.payload.email;
    // },

    // //logdata  admin
    // [logdata.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    // },
    // [logdata.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.authdata = action.payload;
    //   state.error = false;
    // },
    // [logdata.rejected]: (state, action) => {
    //   state.loading = false;
    //   state.error = action.payload;
    // },

    // //getAdmin  admin
    // [getAllAdmins.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    // },
    // [getAllAdmins.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.data = action.payload;
    // },
    // [getAllAdmins.rejected]: (state, action) => {
    //   state.loading = false;
    //   state.error = action.payload;
    // },

    // //getAdminById  admin
    // [getAdminById.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    // },
    // [getAdminById.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.error = false;
    //   state.data = action.payload;
    // },
    // [getAdminById.rejected]: (state, action) => {
    //   state.loading = false;
    //   state.error = action.payload;
    // },

    // //updatedata   admin
    // [updatedata.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    //   state.message = null;
    // },
    // [updatedata.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.message = action.payload;
    // },
    // [updatedata.rejected]: (state, action) => {
    //   state.loading = false;
    //   state.message = action.payload;
    // },

    // //deleteadmin  admin
    // [deleteadmin.pending]: (state, action) => {
    //   state.loading = true;
    //   state.error = false;
    // },
    // [deleteadmin.fulfilled]: (state, action) => {
    //   state.loading = false;
    //   state.error = false;
    //   state.data = state.data.filter((el) => el.id !== action.payload);
    // },
    // [deleteadmin.rejected]: (state, action) => {
    //   state.loading = false;

    //   state.error = action.payload;
    // },

    //login   doctor

    /// doctors *********************//

    [loginDoctor.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [loginDoctor.fulfilled]: (state, action) => {
      state.loading = false;
      state.authdata = action.payload;
    },
    [loginDoctor.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //register
    // [register.pending]: (state, action) => {
    //   state.laodingBTN = true;
    // },
    // [register.fulfilled]: (state, action) => {
    //   state.dataCreate = action.payload;
    //   state.laodingBTN = false;
    //   notifysuccess(action.payload.message);
    //   state.error = false;
    // },
    // [register.rejected]: (state, action) => {
    //   state.laodingBTN = false;
    //   state.error = action.payload;
    //   notifyError(
    //     action.payload?.message && action.payload.message
    //       ? action.payload.message
    //       : action.payload
    //   );
    // },
  },
});

export default authslice.reducer;

export let { logout, datalogfn } = authslice.actions;
