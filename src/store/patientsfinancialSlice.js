import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";
import { BASE_URL } from "../apiConfig"; // Adjust the path based on your project structure
import { notifyError, notifysuccess } from "../Notification";
export let getUserFnaccount = createAsyncThunk(
  "getUserFnaccount/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.get(
        `${BASE_URL}/api/users/getEnableFinancialUser`,
        arg,
        {
          headers: { "Content-Type": "application/json;charset=utf-8" },
        }
      );

      return data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let createfnaccount = createAsyncThunk(
  "createfnaccount/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.post(
        `${BASE_URL}/api/financialAccounts/createFinancialAccount/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getallfnaccount = createAsyncThunk(
  "getallfnaccount/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.get(
        `${BASE_URL}/api/financialAccounts/getAllFinancialAccounts`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//deletet functional
export let deleteFinancialAccount = createAsyncThunk(
  "deleteFinancialAccount/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      const idList = Array.isArray(arg.id) ? arg.id : [arg.id]; // Ensure id is an array
      const promises = idList.map((id) =>
        axios.post(
          `${BASE_URL}/api/financialAccounts/deleteFinancialAccount/${id}`,
          arg.data,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        )
      );

      // Wait for all deletion requests to complete
      const responses = await Promise.all(promises); // Wait for all deletion requests to complete

      return { idList: idList, responses: responses }; // Return the list of deleted IDs
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getUserSessions = createAsyncThunk(
  "getUserSessions/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.get(
        `${BASE_URL}/api/financialAccounts/getUserSessions/${arg}`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//createSession
export let createSession = createAsyncThunk(
  "createSession/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.post(
        `${BASE_URL}/api/sessions/createSession/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );

      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//deleteSession
export let deleteSession = createAsyncThunk(
  "deleteSession/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.delete(
        `${BASE_URL}/api/sessions/deleteSession/${arg}`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return arg;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

//create subSessions
export let createSubSession = createAsyncThunk(
  "createSubSession/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.post(
        `${BASE_URL}/api/subSessions/createSubSession/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//deleteSubSession
export let deleteSubSession = createAsyncThunk(
  "deleteSubSession/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.delete(
        `${BASE_URL}/api/subSessions/deleteSubSession/${arg.id}`,
        arg,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return arg;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//updateSubSession
export let updateSubSession = createAsyncThunk(
  "updateSubSession/acco",
  async (arg, thunkAPI) => {
    let rejectWithValue = { thunkAPI };
    try {
      let data = await axios.post(
        `${BASE_URL}/api/subSessions/updateSubSession/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "application/json;charset=utf-8",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

let accountSlice = createSlice({
  name: "acco",
  initialState: {
    loading: false,
    laodingBTN: false,
    data: [],
    dataCreate: false,
    dataUpdate: false,
    error: false,
    errorAllFn: false,
    datalog: false,
    datasingle: false,
    Allpatient: false,
    dataSetion: false,
    loadingAllpatient: false,
  },
  reducers: {
    resetDataCreateAction: (state, action) => {
      state.dataCreate = action.payload;
    },
  },
  extraReducers: {
    //getUserFnaccount
    [getUserFnaccount.pending]: (state, action) => {
      state.loadingAllpatient = true;

      state.errorAllFn = false;
    },
    [getUserFnaccount.fulfilled]: (state, action) => {
      state.Allpatient = action.payload;
      state.loadingAllpatient = false;
      state.errorAllFn = false;
    },
    [getUserFnaccount.rejected]: (state, action) => {
      state.loadingAllpatient = false;
      state.errorAllFn = true;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //createfnaccount
    [createfnaccount.pending]: (state, action) => {
      state.laodingBTN = true;
      state.dataCreate = false;
      state.error = false;
    },
    [createfnaccount.fulfilled]: (state, action) => {
      state.laodingBTN = false;
      state.dataCreate = action.payload;
      notifysuccess(action.payload.message);
      state.error = false;
    },
    [createfnaccount.rejected]: (state, action) => {
      state.laodingBTN = false;
      state.error = true;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //deleteFinancialAccount
    [deleteFinancialAccount.pending]: (state, action) => {
      state.loadingBTN = true;
    },
    [deleteFinancialAccount.fulfilled]: (state, action) => {
      state.data = state.data.filter(
        (e) => !action.payload.idList.includes(e.id)
      );
      state.loadingBTN = false;
      state.error = false;
      notifysuccess(action.payload.responses[0].data.message);
    },
    [deleteFinancialAccount.rejected]: (state, action) => {
      state.loadingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getallfnaccount
    [getallfnaccount.pending]: (state, action) => {
      state.loading = true;
      state.data = false;
      state.error = false;
    },
    [getallfnaccount.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
      state.error = false;
    },
    [getallfnaccount.rejected]: (state, action) => {
      state.loading = false;
      state.error = true;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getUserSessions
    [getUserSessions.pending]: (state, action) => {
      state.loading = true;
      state.error = false;
    },
    [getUserSessions.fulfilled]: (state, action) => {
      state.dataSetion = action.payload;
      state.loading = false;
      state.error = false;
    },
    [getUserSessions.rejected]: (state, action) => {
      state.loading = false;
      state.error = true;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //createSession
    [createSession.pending]: (state, action) => {
      state.loadingBTN = true;
      state.error = false;
    },
    [createSession.fulfilled]: (state, action) => {
      state.dataCreate = action.payload; // Create a new array with the updated data
      state.loadingBTN = false;
      notifysuccess(action.payload.message);
      state.error = false;
    },
    [createSession.rejected]: (state, action) => {
      state.error = true;
      state.loadingBTN = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //deleteSession
    [deleteSession.pending]: (state, action) => {
      state.laodingBTN = true;
      state.error = false;
    },
    [deleteSession.fulfilled]: (state, action) => {
      state.laodingBTN = false;
      state.dataSetion = state.dataSetion.filter(
        (el) => el.id !== action.payload
      );
      state.error = false;
    },
    [deleteSession.rejected]: (state, action) => {
      state.error = true;
      state.laodingBTN = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //createSubSession
    [createSubSession.pending]: (state, action) => {
      state.loadingBTN = true;
      state.error = false;
    },
    [createSubSession.fulfilled]: (state, action) => {
      state.loadingBTN = false;
      state.dataCreate = action.payload;
      notifysuccess(action.payload.message);
      state.error = false;
    },

    [createSubSession.rejected]: (state, action) => {
      state.error = true;
      state.loadingBTN = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //updateSubSession
    [updateSubSession.pending]: (state, action) => {
      state.loadingBTN = true;
      state.error = false;
    },
    [updateSubSession.fulfilled]: (state, action) => {
      state.loadingBTN = false;
      state.dataUpdate = action.payload;
      notifysuccess(action.payload.message);
      state.error = false;
    },

    [updateSubSession.rejected]: (state, action) => {
      state.error = true;
      state.loadingBTN = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //deleteSubSession
    [deleteSubSession.pending]: (state, action) => {
      state.laodingBTN = true;
      state.error = false;
    },
    [deleteSubSession.fulfilled]: (state, action) => {
      state.laodingBTN = false;

      const getsesion = state.dataSetion;

      let foundIndex = getsesion.findIndex(
        (item) => item.id === action.payload.sesion_id
      );

      if (foundIndex !== -1) {
        // Find the subSession to be deleted
        const subSessionToDelete = getsesion[foundIndex].subSessions.data.find(
          (e) => e.id === action.payload.id
        );

        if (subSessionToDelete) {
          // Update the 'paid' value in the parent session
          getsesion[foundIndex].paid -= subSessionToDelete.paid;
          getsesion[foundIndex].remaining_cost += subSessionToDelete.paid;

          // Remove the subSession from the 'data' array
          getsesion[foundIndex].subSessions.data = getsesion[
            foundIndex
          ].subSessions.data.filter((e) => e.id !== action.payload.id);
        }
      }

      state.dataSetion = getsesion;
      notifysuccess("Delete Success");
      state.error = false;
    },
    [deleteSubSession.rejected]: (state, action) => {
      state.error = true;
      state.laodingBTN = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
  },
});

export default accountSlice.reducer;
export let { resetDataCreateAction } = accountSlice.actions;
