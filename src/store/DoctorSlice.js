import axios from "axios";

import { BASE_URL } from "../apiConfig";
import { notifyError, notifysuccess } from "../Notification";
const { createSlice, createAsyncThunk } = require("@reduxjs/toolkit");

//************************doctors************************************//
export let registerDoctor = createAsyncThunk(
  "auth/registerDoctor",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let res = await axios.post(
        `${BASE_URL}/api/doctors/registerDoctor`,
        arg,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return res.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

//************************doctors************************************//
export let getAllDoctors = createAsyncThunk(
  "auth/getAllDoctors",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let res = await axios.get(`${BASE_URL}/api/doctors/getAllDoctors`, arg, {
        headers: { "Content-Type": "application/json" },
      });
      return res.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let getDoctorById = createAsyncThunk(
  "auth/getDoctorById",
  async (arg, thunkAPI) => {
    let { rejectWithValue } = thunkAPI;
    try {
      let res = await axios.get(
        `${BASE_URL}/api/doctors/getDoctorById/${arg}`,
        arg,
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      return res.data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let updateDoctor = createAsyncThunk(
  "doctors/updateDoctor",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      let data = await axios.post(
        `${BASE_URL}/api/doctors/updateDoctor/${arg.id}`,
        arg.data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      return data.data;
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

export let deleteDoctor = createAsyncThunk(
  "doctors/deleteDoctor",
  async (arg, ThunkAPI) => {
    let { rejectWithValue } = ThunkAPI;
    try {
      const idList = Array.isArray(arg.id) ? arg.id : [arg.id]; // Ensure id is an array
      const promises = idList.map((id) =>
        axios.post(`${BASE_URL}/api/doctors/deleteDoctor/${id}`, arg.data, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        })
      );

      // Wait for all deletion requests to complete
      const responses = await Promise.all(promises); // Wait for all deletion requests to complete

      return { idList: idList, responses: responses }; // Return the list of deleted IDs
    } catch (error) {
      return rejectWithValue(
        error.response ? error.response.data : error.message
      );
    }
  }
);

let doctorSlice = createSlice({
  name: "auth",
  initialState: {
    data: false,
    dataAll: false,
    authdata: false,
    loading: false,
    error: false,
    message: false,
    datalog: false,
    loadingDoc: false,
    createDoc: false,
    dataSingle: false,
    laodingBTN: false,
    laodingSingle: false,
    dataCreate: false,
    errorSingle: false,
  },
  reducers: {
    resetDataCreateAction: (state, action) => {
      state.dataCreate = action.payload;
    },
    resetDataUpdateAction: (state, action) => {
      state.dataUpdate = action.payload;
    },
  },
  extraReducers: {
    /// doctors *********************//
    //getAllDoctors  doctor

    [getAllDoctors.pending]: (state, action) => {
      state.loading = true;
      state.dataCreate = false;
    },
    [getAllDoctors.fulfilled]: (state, action) => {
      state.data = action.payload;
      state.loading = false;
      state.error = false;
      state.dataCreate = false;
    },
    [getAllDoctors.rejected]: (state, action) => {
      state.loading = false;
      state.error = action.payload;
      state.dataCreate = false;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //registerDoctor
    [registerDoctor.pending]: (state, action) => {
      state.laodingBTN = true;
    },
    [registerDoctor.fulfilled]: (state, action) => {
      state.dataCreate = action.payload;
      state.laodingBTN = false;
      notifysuccess(action.payload.message);
      state.error = false;
    },
    [registerDoctor.rejected]: (state, action) => {
      state.laodingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //updateDoctor
    [updateDoctor.pending]: (state, action) => {
      state.laodingBTN = true;
      state.error = false;
    },
    [updateDoctor.fulfilled]: (state, action) => {
      state.laodingBTN = false;
      state.dataUpdate = action.payload;
      notifysuccess(action.payload.message);
    },
    [updateDoctor.rejected]: (state, action) => {
      state.laodingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //getDoctorById  doctor

    [getDoctorById.pending]: (state, action) => {
      state.laodingSingle = true;
      state.errorSingle = false;
    },
    [getDoctorById.fulfilled]: (state, action) => {
      state.dataSingle = action.payload;
      state.laodingSingle = false;
      state.errorSingle = false;
    },
    [getDoctorById.rejected]: (state, action) => {
      state.laodingSingle = false;
      state.errorSingle = true;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },

    //deleteDoctor
    [deleteDoctor.pending]: (state, action) => {
      state.loadingBTN = true;
    },
    [deleteDoctor.fulfilled]: (state, action) => {
      state.data = state.data.filter(
        (e) => !action.payload.idList.includes(e.id)
      );
      state.loadingBTN = false;
      state.error = false;
      notifysuccess(action.payload.responses[0].data.message);
    },
    [deleteDoctor.rejected]: (state, action) => {
      state.loadingBTN = false;
      state.error = action.payload;
      notifyError(
        action.payload?.message && action.payload.message
          ? action.payload.message
          : action.payload
      );
    },
  },
});

export default doctorSlice.reducer;
export let { resetDataCreateAction, resetDataUpdateAction } =
  doctorSlice.actions;
