import Home from "./Pages/Home/Home";
import MainUser from "./Pages/Folder-users/MainUser/MainUser";
import EditCreateUser from "./Pages/Folder-users/EditCreateUser/EditCreateUser";
import MainAppointments from "./Pages/Folder-appointments/MainAppointments/MainAppointments";
import EditCreateAppointment from "./Pages/Folder-appointments/EditCreateAppointment/EditCreateAppointment";
import ShowAppointment from "./Pages/Folder-appointments/ShowAppointment/ShowAppointment";
import MainDoctors from "./Pages/Folder-Doctors/MainDoctors/MainDoctors";
import EditCreateDoctros from "./Pages/Folder-Doctors/EditCreateDoctros/EditCreateDoctros";
import MainPatientsFinancial from "./Pages/Folder-Accounting/Folder-Patients-financial/MainPatientsFinancial/MainPatientsFinancial";
import CreateSessionPatiensFinancial from "./Pages/Folder-Accounting/Folder-Patients-financial/CreateSessionPatiensFinancial/CreateSessionPatiensFinancial";
import EditSessionSubPatiensFinancial from "./Pages/Folder-Accounting/Folder-Patients-financial/EditSessionSubPatiensFinancial/EditSessionSubPatiensFinancial";
import Login from "./Pages/login/Login";
import NotFound from "./Pages/NotFoundPage/NotFound";
import EccoStore from "./Pages/FolderEcoo/EccoStore/EccoStore";
import OrderEcco from "./Pages/FolderEcoo/OrderEcco/OrderEcco";
import Tooth from "./Pages/TestChose/Test";
import RoleAndPermision from "./Pages/Folder-role/RoleAndPermision/RoleAndPermision";
import RolePermEditAdd from "./Pages/Folder-role/RolePermisionEditAdd/RolePermEditAdd";

const useRout = (token) => {
  const routes = [
    {
      path: "/",
      element: token ? <Home /> : <Login />,
    },
    {
      path: "doctors",
      element: <MainDoctors />,
    },
    {
      path: "doctors/:name",
      element: <EditCreateDoctros />,
    },
    {
      path: "doctors/:name/:id",
      element: <EditCreateDoctros />,
    },
    {
      path: "users",
      element: <MainUser />,
    },
    {
      path: "users/:name",
      element: <EditCreateUser />,
    },
    {
      path: "users/:name/:id",
      element: <EditCreateUser />,
    },
    {
      path: "appointments",
      element: <MainAppointments />,
    },
    {
      path: "appointments/show/:id",
      element: <ShowAppointment />,
    },
    {
      path: "appointments/:name",
      element: <EditCreateAppointment />,
    },
    {
      path: "appointments/:name/:id",
      element: <EditCreateAppointment />,
    },
    { path: "roles-permision", element: <RoleAndPermision /> },
    { path: "roles-permision/:name/:id", element: <RolePermEditAdd /> },
    { path: "roles-permision/:name", element: <RolePermEditAdd /> },
    {
      path: "Patients-financial",
      element: <MainPatientsFinancial />,
    },
    {
      path: "Patients-financial/:name",
      element: <CreateSessionPatiensFinancial />,
    },
    {
      path: "Patients-financial/:name/:id/:user_id",
      element: <EditSessionSubPatiensFinancial />,
    },
    { path: "/store", element: <Tooth /> },
    { path: "/order", element: <OrderEcco /> },
    { path: "*", element: <NotFound /> },
  ];
  return routes;
};
export default useRout;
